-- Create a role for kugelblitz and set grants to tables that can be
-- configured and visualized by the application

-- Only create kugelblitz if it doesn't exist yet. It may already exist
-- when one tries to, for instance, create several instances of the
-- database in the same postgresql cluster.
DO $$
BEGIN
    IF NOT EXISTS (SELECT * FROM pg_roles WHERE rolname = 'kugelblitz') THEN
        CREATE ROLE kugelblitz;
        ALTER ROLE kugelblitz
              WITH NOSUPERUSER INHERIT NOCREATEROLE NOCREATEDB LOGIN
	           NOREPLICATION NOBYPASSRLS PASSWORD 'secret';
    END IF;
END
$$;

GRANT USAGE ON SCHEMA users TO kugelblitz;
GRANT USAGE ON SCHEMA blitz TO kugelblitz;
GRANT USAGE ON SCHEMA alarm TO kugelblitz;

GRANT SELECT ON ALL TABLES IN SCHEMA users TO kugelblitz;
GRANT SELECT ON ALL TABLES IN SCHEMA alarm TO kugelblitz;
GRANT SELECT ON ALL TABLES IN SCHEMA blitz TO kugelblitz;
GRANT USAGE, SELECT ON ALL SEQUENCES IN SCHEMA users TO kugelblitz;
GRANT USAGE, SELECT ON ALL SEQUENCES IN SCHEMA alarm TO kugelblitz;
GRANT USAGE, SELECT ON ALL SEQUENCES IN SCHEMA blitz TO kugelblitz;

GRANT SELECT,INSERT,DELETE,UPDATE ON TABLE users.users TO kugelblitz;
GRANT SELECT,INSERT,DELETE,UPDATE ON TABLE users.groups TO kugelblitz;
GRANT SELECT,INSERT,DELETE,UPDATE ON TABLE users.roles TO kugelblitz;
GRANT SELECT,INSERT,DELETE,UPDATE ON TABLE users.user_roles TO kugelblitz;
GRANT SELECT,INSERT,DELETE,UPDATE ON TABLE users.role_permissions  TO kugelblitz;
GRANT SELECT,INSERT,DELETE,UPDATE ON TABLE users.themes TO kugelblitz;
GRANT SELECT,INSERT,DELETE,UPDATE ON TABLE users.app_settings TO kugelblitz;
GRANT SELECT,INSERT,DELETE,UPDATE ON TABLE users.alarm_settings TO kugelblitz;
GRANT SELECT,INSERT,DELETE,UPDATE ON TABLE users.profile TO kugelblitz;
GRANT SELECT,INSERT,DELETE,UPDATE ON TABLE users.sessions TO kugelblitz;
GRANT SELECT,INSERT,DELETE,UPDATE ON TABLE users.session_log TO kugelblitz;
GRANT SELECT,INSERT,DELETE,UPDATE ON TABLE users.map_layers TO kugelblitz;
GRANT SELECT,INSERT,DELETE,UPDATE ON TABLE users.group_map_layer TO kugelblitz;
GRANT SELECT,INSERT ON TABLE users.logging TO kugelblitz;

GRANT SELECT,INSERT,DELETE,UPDATE ON TABLE alarm.alarms TO kugelblitz;
GRANT SELECT,INSERT,DELETE,UPDATE ON TABLE alarm.alarm_area_unit TO kugelblitz;
GRANT SELECT,INSERT,DELETE,UPDATE ON TABLE alarm.alarm_area_map TO kugelblitz;
GRANT SELECT,INSERT,DELETE,UPDATE ON TABLE alarm.alarm_areas TO kugelblitz;
GRANT SELECT,INSERT,DELETE ON TABLE alarm.stroke_counter TO kugelblitz;

GRANT SELECT,INSERT,DELETE,UPDATE ON TABLE blitz.stroke_areas TO kugelblitz;



