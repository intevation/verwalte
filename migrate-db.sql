-- Migrate data from the old database
--
-- Usage:
-- psql -d newdb --set=olddbname='OLDDB' [ --set=superuser='ROLE' ] -f migrate-db.sql
--
--   By default the superuser (postgres in the old blids db is mapped to
--   postgres in the new db.  If as a deveoper a individual account is
--   used, the superuser variable can be used to tweak this mapping...

\if :{?superuser}
  -- noop
  -- sorry I'm not able to get this working any more elegant...  :-(
\else
  \set superuser postgres
\endif

-- With ON_ERROR_STOP, psql will stop processing the file as soon as an
-- error occurs.
\set ON_ERROR_STOP

-- The overall strategy is to make the old database available in the new
-- one using foreign data wrappers. Then we can transfer and transform
-- the data directly with a suitable sequence of SQL statements.
--
-- For the foreign data wrapper the code assumes that it possible to
-- access the old database as the user postgres without any other
-- parameters.
--
-- We try to re-use the old IDs so that we don't have to complicate the
-- logic with code to translate between different sets of IDs. A
-- consequence is that the sequences for the default values for the IDs
-- will not be updated automatically during the migration and will have
-- to be adjusted explicitly, preferably immediately after the insert so
-- that subsequent modifications can assume that inserts without
-- explicit ids will work as expected.
--
-- Some of the data in the old database does not need to be migrated:
--
--  - benutzer.themes, benutzer.host_themes
--
--    Theming will be dones in a completely different way and configured
--    manually after the migration using users.host_theme and
--    users.themes.
--
--  - details of benutzer.rollen
--
--    The roles as such and the n:m-relation between users and roles are
--    migrated. The details about which permissions which role has are
--    not migrated. They are expected to be filled in manually after the
--    migration using the new tables users.activity, users.permissions,
--    users.permission_activity, and users.role_permissions.

--
-- TODO: Implement migration for the following tables or decide how else
-- to deal with them
--
-- Tables in the new schema which aren't populated by the migration
-- script so far, excluding those explicitly mentioned above as not
-- being handled by this script:
--
--     blitz.compare
--     blitz.strokes
--        Both have obvious mappings, from blitz.vergleich and
--        blitz.blitze respectively.
--        Will probably be handled separately.
--
--     users.alarm_config
--        Needs clarification.
--
--     users.profile
--        Will be created by the new implementation on demand.
--
--     users.session_log
--         Can be empty for new application
--
--     users.sessions
--         Obvious mapping from benutzer.session.
--         Can be empty for new application


--
-- Step 0: Prepare database
--
-- Everything CREATEd in this step will need to be DROPped again at the
-- end. Other parts of this script should only create tables, functions,
-- etc. in the schemas created here.
--

-- Create extensions
CREATE EXTENSION postgres_fdw;

-- pgcrypto for UUIDs.
CREATE EXTENSION pgcrypto;


-- Create temporary schemas and data types

-- Schemas to hold the mapped tables of the old database. The names are
-- the names of the relevant schemas of the old database prefixed with
-- "olddb_".
CREATE SCHEMA olddb_alarmserver;
CREATE SCHEMA olddb_benutzer;

-- Schema and types referenced by the mapped tables.
-- Due to the way postgres_fdw works, we need to have the same types
-- with the same names in the current database.

CREATE SCHEMA alarmserver;

CREATE TYPE alarmserver.alarmtyp AS ENUM (
    'Alarm',
    'Entwarnung',
    'Vorwarnung',
    'Testalarm',
    'Testentwarnung',
    'Testvorwarnung'
);
CREATE TYPE alarmserver.send_state AS ENUM (
    'ignored',
    'success',
    'partial success',
    'error'
);


--
-- Create some helper functions
--

-- Convert timestamp/UTC-flag pair to a UTC time stamp.
--
-- The old database uses time stamps together with a UTC flag in some
-- places. If the flag is false, the actual time zone is CET, otherwise
-- it's UTC. This function replicates how the conversion is implemented
-- in the old database in alarmserver.notify_alarm().

CREATE FUNCTION olddb_benutzer.convert_time_stamp(
    datetime TIMESTAMP WITH TIME ZONE,
    utc BOOLEAN)
RETURNS TIMESTAMP WITH TIME ZONE
LANGUAGE plpgsql
STABLE
AS $$
DECLARE
    tz TEXT;
BEGIN
    IF utc THEN
        tz := 'UTC';
    ELSE
        tz := 'Europe/Berlin';
    END IF;

    RETURN datetime :: TIMESTAMP AT TIME ZONE tz;
END;
$$;


-- Convert the enums used in the old schema to the enums used in the new
-- schema.

CREATE FUNCTION olddb_alarmserver.convert_alarmtyp(old alarmserver.alarmtyp)
RETURNS alarm.alarm_type
LANGUAGE plpgsql
STABLE
AS $$
BEGIN
    CASE old
        WHEN 'Alarm'          THEN RETURN 'alarm';
        WHEN 'Entwarnung'     THEN RETURN 'clear';
        WHEN 'Vorwarnung'     THEN RETURN 'prewarning';
        WHEN 'Testalarm'      THEN RETURN 'testalarm';
        WHEN 'Testentwarnung' THEN RETURN 'testclearing';
        WHEN 'Testvorwarnung' THEN RETURN 'testprewarning';
    END CASE;
END;
$$;


CREATE FUNCTION olddb_alarmserver.convert_send_state(old alarmserver.send_state)
RETURNS alarm.send_state
LANGUAGE plpgsql
STABLE
AS $$
BEGIN
    RETURN (old :: TEXT) :: alarm.send_state;
END;
$$;

--
-- Step 1: Create Foreign Data Wrapper
--
-- Create the foreign data wrapper for the old database and import the
-- necessary aparts of the schema.
--

CREATE SERVER olddb
 FOREIGN DATA WRAPPER postgres_fdw
              OPTIONS (dbname :'olddbname');
-- DROP SERVER olddb CASCADE;


CREATE USER MAPPING FOR :superuser
                 SERVER olddb
                OPTIONS (user 'postgres');
-- DROP USER MAPPING FOR postgres SERVER olddb;


IMPORT FOREIGN SCHEMA alarmserver
             LIMIT TO (kunden, teilnehmer, kundengruppen, alarme, meldungen,
                       gebiete, gebiet2gruppe, gebietgruppen)
          FROM SERVER olddb
                 INTO olddb_alarmserver;
-- DROP FOREIGN TABLE olddb_alarmserver.teilnehmer;
-- DROP FOREIGN TABLE olddb_alarmserver.meldungen;


IMPORT FOREIGN SCHEMA benutzer
             LIMIT TO (benutzer, gruppen, ebenen, gruppen_ebenen, gruppe2alarm,
                       rollen, berechtigungen)
          FROM SERVER olddb
                 INTO olddb_benutzer;
-- DROP FOREIGN TABLE olddb_benutzer.benutzer;
-- DROP FOREIGN TABLE olddb_benutzer.gruppen;


--
-- Step 2: Transfer the Data
--


--
-- Transfer roles
--
-- We keep the IDs so that it's easier to transfer the user/role
-- relationship.
--
-- We do not transfer the actual permissions that are also stored in the
-- rollen table in the old database. The new permission system is
-- different enough that an automated transfer would be difficult to
-- implement and there are only few roles so it will be easier to create
-- the right mapping of roles to permissions and actions manually.

INSERT INTO users.roles (id, name)
     SELECT id, rollenname as name
       FROM olddb_benutzer.rollen;

-- Make sure inserts without explict IDs will not lead to conflicts
SELECT setval('users.roles_id_seq', (SELECT MAX(id) FROM users.roles));

--
-- Transfer the user groups.
--
-- The combination of (vatergruppe, gruppenname) is not unique in the
-- old table benutzer.gruppen. Fortunately there are only a few
-- duplicates and in all cases the (vatergruppe, gruppenname) pair
-- occurs only twice. To cope with this, we first transfer all old
-- groups that can be inserted into users.groups without conflict and
-- then try again with the rest of the groups while appending '(2)' to
-- the name to make it unique.
--
-- The second step fails with an error if appending '(2)' does not make
-- all names unique. In that case the strategy will have to be revised a
-- little.
--
-- A complication is that users.user_roles requires a role_id for each
-- group. We try to determine which role might be appropriate for a
-- group by looking at the users in that group and its child groups.
-- There are some groups without users. We handle those with a new role.

-- Determine the roles for the groups with the help of some temporary
-- tables.

-- Mapping from group IDs to an array of all roles that are associated
-- with any user who is directly or indirectly a member of the group
CREATE TABLE olddb_benutzer.gruppen_rollen (
    gruppen_id INTEGER NOT NULL UNIQUE,
    rollen INTEGER[] NOT NULL
);

WITH RECURSIVE gruppen_rec(benutzer_id, gruppen_id)
    AS (SELECT id, home_gruppen_id FROM olddb_benutzer.benutzer
        UNION ALL
        SELECT r.benutzer_id, vatergruppe
          FROM gruppen_rec r
          JOIN olddb_benutzer.gruppen g ON r.gruppen_id = g.id
         WHERE vatergruppe IS NOT NULL)
INSERT INTO olddb_benutzer.gruppen_rollen(gruppen_id, rollen)
     SELECT g.gruppen_id, array_agg(distinct rollen_id) as rollen
       FROM gruppen_rec g
       JOIN olddb_benutzer.berechtigungen p ON p.benutzer_id = g.benutzer_id
       JOIN olddb_benutzer.benutzer b ON b.id = g.benutzer_id
      WHERE p.gruppen_id = b.home_gruppen_id
   GROUP BY g.gruppen_id;


-- Mapping of role arrays to role names.
-- Three cases:
--  1. exactly one group for a role array with multiple roles
--     -> use the group name as role name
--  2. multiple roles: concatenate the names of the roles.
--  3. no role: New role 'group_without_users'
--
-- A consequence of 2. is, that a role array with exactly one role will
-- be mapped to the name of the role.

CREATE TABLE olddb_benutzer.rollen_namen (
    rollen INTEGER[] NOT NULL UNIQUE,
    name VARCHAR NOT NULL UNIQUE
);


-- Create new roles for all role arrays that have more than one element.
-- For new entries with exactly one group, we reuse the group name as
-- the role name. In the other cases we concatenate the role names.
WITH distinct_rollen(rollen, gruppen)
  AS (SELECT rollen, array_agg(gruppen_id) as gruppen
        FROM olddb_benutzer.gruppen_rollen
    GROUP BY rollen)
INSERT INTO olddb_benutzer.rollen_namen (rollen, name)
     SELECT rollen,
            CASE WHEN array_length(gruppen, 1) = 1
                  AND array_length(rollen, 1) > 1
                 THEN (SELECT gruppenname
                         FROM olddb_benutzer.gruppen
                        WHERE id = gruppen[1])
                 ELSE array_to_string(ARRAY(SELECT name
                                              FROM users.roles
                                             WHERE id = ANY (rollen)
                                          ORDER BY id), ',')
            END as name
       FROM distinct_rollen;


INSERT INTO users.roles (name)
     SELECT name
       FROM olddb_benutzer.rollen_namen
      WHERE array_length(rollen, 1) > 1;


-- Handle groups without users, i.e. the ones where we couldn't
-- determine the role based on the users's roles.

-- Create new role for groups without users.
INSERT INTO users.roles (name) VALUES ('group_without_users');

-- Assign this name to the empty role array.
INSERT INTO olddb_benutzer.rollen_namen (rollen, name)
     VALUES ('{}', 'group_without_users');

-- Assign the empty role array to all groups which don't have a role
-- array yet.
INSERT INTO olddb_benutzer.gruppen_rollen (gruppen_id, rollen)
     SELECT g.id, '{}'
       FROM olddb_benutzer.gruppen g
      WHERE NOT EXISTS (SELECT * FROM olddb_benutzer.gruppen_rollen gr
                         WHERE gr.gruppen_id = g.id);



-- Now transfer the groups.
WITH group_roles(group_id, role_name)
     AS (SELECT r.gruppen_id, n.name
           FROM olddb_benutzer.gruppen_rollen r
           JOIN olddb_benutzer.rollen_namen n
             ON r.rollen = n.rollen)
INSERT INTO users.groups (id, parent, name, role_id, max_alarmarea)
     SELECT id, vatergruppe as parent, gruppenname as name,
            (SELECT ur.id FROM users.roles ur
              WHERE ur.name = r.role_name) as role_id,
            max_alarmarea
       FROM olddb_benutzer.gruppen old_groups
       JOIN group_roles r ON old_groups.id = r.group_id
ON CONFLICT DO NOTHING;

WITH group_roles(group_id, role_name)
     AS (SELECT r.gruppen_id, n.name
           FROM olddb_benutzer.gruppen_rollen r
           JOIN olddb_benutzer.rollen_namen n
             ON r.rollen = n.rollen)
INSERT INTO users.groups (id, parent, name, role_id, max_alarmarea)
     SELECT id, vatergruppe as parent, gruppenname || '(2)' as name,
            (SELECT ur.id FROM users.roles ur
              WHERE ur.name = r.role_name) as role_id,
            max_alarmarea
       FROM olddb_benutzer.gruppen old_groups
       JOIN group_roles r ON old_groups.id = r.group_id
      WHERE NOT EXISTS (SELECT * FROM users.groups new_groups
                                WHERE new_groups.id = old_groups.id);

-- Make sure inserts without explict IDs will not lead to conflicts
SELECT setval('users.groups_id_seq', (SELECT MAX(id) FROM users.groups));

--
-- Transfer the group's blitzgebiet as stroke_area
--
-- We re-use the group IDs as the IDs of the stroke_area tuples.
--
INSERT INTO blitz.stroke_areas (id, group_id, geom)
     SELECT id, id as group_id, blitzgebiet as geom
       FROM olddb_benutzer.gruppen old_groups
      WHERE EXISTS (SELECT * FROM users.groups new_groups
                            WHERE new_groups.id = old_groups.id);
-- Make sure inserts without explict IDs will not lead to conflicts
SELECT setval('blitz.stroke_areas_id_seq',
              (SELECT MAX(id) FROM blitz.stroke_areas));


--
-- Transfer the group's statistikgebiet as data_area
--
-- We re-use the group IDs as the IDs of the stroke_area tuples.
--
-- TODO: Verify that data_area really is the former statistikgebiet.
INSERT INTO blitz.data_areas (id, geom)
     SELECT id, statistikgebiet as geom
       FROM olddb_benutzer.gruppen old_groups
      WHERE statistikgebiet IS NOT NULL
        AND EXISTS (SELECT * FROM users.groups new_groups
                            WHERE new_groups.id = old_groups.id);
-- Make sure inserts without explict IDs will not lead to conflicts
SELECT setval('blitz.data_areas_id_seq', (SELECT MAX(id) FROM blitz.data_areas));

UPDATE users.groups g
   SET data_area_id = (SELECT id FROM blitz.data_areas d WHERE d.id = g.id);


--
-- Transfer the app settings associated with the groups
--
-- We re-use the group ids as the ids of the app_settings tuples.
INSERT INTO users.app_settings (id, group_id, max_display_time,
                                archive_days, archive_since,
                                max_zoom, min_zoom, live_blids,
                                animation, sound, path,
                                max_alarms, max_alarm_areas, max_alarm_users,
                                statistic_windowed)
     SELECT id, id as group_id, max_displayzeit as max_display_time,
            archiv_tage as archive_days, archiv_ab as archive_since,
            max_zoom, min_zoom, liveblids as live_blids,
            animation, sound, pfad as path,
            max_alarme as max_alarms, max_alarmgebiete as max_alarm_areas,
            max_alarmteilnehmer as max_alarm_users,
            statistik_windowed as statistic_windowed
       FROM olddb_benutzer.gruppen old_groups
      WHERE EXISTS (SELECT * FROM users.groups new_groups
                             WHERE new_groups.id = old_groups.id);

-- Make sure inserts without explict IDs will not lead to conflicts
SELECT setval('users.app_settings_id_seq',
              (SELECT MAX(id) FROM users.app_settings));


--
-- Transfer users
--
-- We reuse the user IDs from the old database.
--
-- The group referenced by home_gruppen_id of the old database may not
-- have been migrated earlier if we couldn't determine the role for the
-- group (which likely means it didn't have any relevant users).
-- At the moment, there are only two users that are not transferred, and
-- both appear to be test users.
--
-- TODO: verify that only test users are not transferred in this step.
-- This should be done as late as possible because it's dependent on how
-- other parts the data are transferred.
INSERT INTO users.users (id, name, password, group_id, start_date, stop_date,
                         first_name, last_name, company, division, street,
                         zip, city, phonenumber, annotation, mobilenumber,
                         email)
     SELECT b.id,
            benutzername as name,
            passwort as password,
            home_gruppen_id as group_id,
            startdatum as start_date,
            stopdatum as stop_date,
            COALESCE(firstname,'') as first_name,
            COALESCE(lastname,'') as last_name,
            COALESCE(company,'') as company,
            COALESCE(division,'') as division,
            COALESCE(streetadress,'') as street,
            COALESCE(zip,'') as zip,
            COALESCE(city,'') as city,
            COALESCE(phonenumber,'') as phonenumber,
            COALESCE(annotation,'') as annotation,
            COALESCE(mobilenumber,'') as mobilenumber,
            COALESCE(email,'') as email
       FROM olddb_benutzer.benutzer b
       JOIN users.groups g ON home_gruppen_id = g.id;

-- Make sure inserts without explict IDs will not lead to conflicts
SELECT setval('users.users_id_seq', (SELECT MAX(id) FROM users.users));


--
-- Transfer user roles
--

-- In the old database the table berechtigungen maps user/group pairs to
-- roles. This suggests that the roles are related to user's group in
-- some way. However, the SQL-statements in the Go-code that accesses
-- this, do not care much about the groups at all. They merely require
-- that the group in the berechtigungen table exists. They do not
-- require that they're the group the user belongs to or one of that
-- group's parent groups. We follow the same logic here.
--
-- What makes the transfer easier is that in the old database,
-- berechtigungen contains exactly one row for each user.
--
-- We don't join the benutzer and rollen tables because that shouldn't
-- be necessary. If benutzer_id or rollen_id refer to non-existing
-- entries, the INSERT will fail because of unmet foreign key
-- constraints.
--
-- TODO: Verify that following the logic of the Go-code is correct.
INSERT INTO users.user_roles (user_id, role_id)
     SELECT benutzer_id as user_id,
            rollen_id as role_id
       FROM olddb_benutzer.berechtigungen b
       JOIN olddb_benutzer.gruppen g ON g.id = b.gruppen_id
      WHERE EXISTS (SELECT * FROM users.users u WHERE u.id = benutzer_id);


-- Transfer Customers/Participants
--
-- Strategy: customers ("kunden") become groups with a shared parent
-- group. Participants ("teilnehmer") become users. Both customers and
-- participants will get new IDs during this step, so we create some new
-- tables to keep track of the mapping.

-- We also need to create the new parent group and a new role for the
-- customer groups.

-- TODO: decide whether having one special role for all customer groups
-- is the right thing to do.

-- Create the customer role
INSERT INTO users.roles (name) VALUES ('Customer');

CREATE FUNCTION olddb_alarmserver.customer_role() RETURNS BIGINT STABLE
LANGUAGE plpgsql AS $$
BEGIN
   RETURN (SELECT id FROM users.roles WHERE name = 'Customer');
END
$$;

-- Create the customer parent group.
INSERT INTO users.groups (name, role_id)
     VALUES ('Customers', olddb_alarmserver.customer_role());

CREATE FUNCTION olddb_alarmserver.customer_group() RETURNS BIGINT STABLE
LANGUAGE plpgsql AS $$
BEGIN
   RETURN (SELECT id FROM users.groups WHERE name = 'Customers');
END
$$;


-- Create ID mapping tables and helper functions to make the mapping
-- easier to use.
CREATE TABLE olddb_alarmserver.kunde_id_mapping (
    kunde_id BIGINT NOT NULL UNIQUE,
    group_id BIGINT NOT NULL UNIQUE,
    parent_id BIGINT
);

CREATE FUNCTION olddb_alarmserver.group_from_kunde(kunde BIGINT) RETURNS BIGINT
STABLE LANGUAGE plpgsql AS $$
BEGIN
   RETURN (SELECT group_id
             FROM olddb_alarmserver.kunde_id_mapping
            WHERE kunde_id = kunde);
END
$$;

CREATE FUNCTION olddb_alarmserver.parent_from_kunde(kunde BIGINT) RETURNS BIGINT
STABLE LANGUAGE plpgsql AS $$
BEGIN
   RETURN (SELECT parent_id
             FROM olddb_alarmserver.kunde_id_mapping
            WHERE kunde_id = kunde);
END
$$;


CREATE TABLE olddb_alarmserver.teilnehmer_id_mapping (
    teilnehmer_id BIGINT NOT NULL UNIQUE,
    user_id BIGINT NOT NULL UNIQUE -- REFERENCES users.users(id)
);

CREATE FUNCTION olddb_alarmserver.user_from_teilnehmer(teilnehmer BIGINT)
RETURNS BIGINT
STABLE LANGUAGE plpgsql AS $$
BEGIN
   RETURN (SELECT user_id
             FROM olddb_alarmserver.teilnehmer_id_mapping
            WHERE teilnehmer_id = teilnehmer);
END
$$;


--
-- Transfer "kunden" as groups.
--

-- Create ID-mapping
--
-- We cannot use the original IDs because the groups table already
-- contains the migrated groups. To work out the mapping of the IDs we
-- fill kunde_id_mapping using nextval with the groups tables ID
-- sequence. We can then use this mapping when creating the groups
-- entries.
INSERT INTO olddb_alarmserver.kunde_id_mapping(kunde_id, group_id)
     SELECT id, nextval('users.groups_id_seq')
       FROM olddb_alarmserver.kunden;

-- Determine the parent group for each of the customer groups.
--
-- For the migration we create a new group for the customer and try to
-- determine which group to use as the parent group. Which group we
-- choose is recorded in kunde_id_mapping as parent_id.

-- Determining which group to use as the parent is a bit complicated:
--
-- In the old database, many, but not all, "kunden" are associated with
-- groups with the benutzer.gruppe2alarm table, so we should try to keep
-- that association in tact in the migration. However, this isn't easy
-- because some customers are associated with multiple groups and some
-- of the groups referenced in gruppe2alarm do not actually exist in the
-- benutzer.gruppen table. Therefore we need to considere several cases:
--
--  (a) customer is not referenced in gruppe2alarm
--      -> use the new 'Customers' group as the parent
--
--  (b) customer is referenced, and is associated with
--
--      (b.1) one or more existing groups
--
--            (b.1.1) one of which is the most specific
--                    -> use that one group
--
--                    This case also handles the most obvious case: that
--                    of the customer being associated with exactly one
--                    existing group.
--
--                    The "most specific" group is the one which has all
--                    the other zero or more groups as ancestors.
--
--            (b.1.2) none of which is the most specific
--                    -> TODO: What to do in this case?
--                       For now, we ignore these customers
--
--      (b.2) both, existing and non-existing groups
--            -> ignore the non-existing ones and treat like (b.1)
--
--      (b.3) only non-existing groups
--            -> Treat like (a)
--        
-- The gruppe2alarm table in the old database has some entries that
-- reference non-existing entries in alarmserver.kunden. We can safely
-- ignore those because we don't migrate non-existing kunden.
--
-- TODO: Choose what to do with case (b.1.2) above
-- TODO: Verify that our choice of parent group is OK

-- To help with the choice of parent group (case (b.1) above), we need
-- to determine the most specific group of a set of groups. We create a
-- custom aggregate function for this. The main idea is that, for each
-- group g, we determine the path p(g) from that group along the chain
-- of parents to the top-most group. That path is represented as an
-- array containing the IDs of the groups in the path. With that we can
-- decide which one of two given groups g1 and g2 is the most specific
-- one:
--
--    if p(g1) contains p(g2), g1 is more specific,
--    otherwise,
--    if p(g2) contains p(g1), g2 is more specific,
--    otherwise,
--    neither is more specific than the other.
--
-- This logic is implemented in olddb_alarmserver.group_path_agg_state.
--
-- To determine which path of a set of paths is the most specific, we
-- can just apply the above binary operation on pair-wise in any order,
-- using the empty path as the neutral element (any other path is more
-- specific than the empty path) and NULL to represent the fact that no
-- path is the most specific one.

CREATE OR REPLACE FUNCTION olddb_alarmserver.group_path_agg_state(
    path_1 INTEGER[],
    path_2 INTEGER[]
)
RETURNS INTEGER[]
LANGUAGE plpgsql
STABLE
AS $$
BEGIN
    IF path_1 IS NULL OR path_2 IS NULL THEN
        RETURN NULL;
    END IF;

    IF path_1 @> path_2 THEN
        RETURN path_1;
    ELSIF path_2 @> path_1 THEN
        RETURN path_2;
    ELSE
        RETURN NULL;
    END IF;
END;
$$;


CREATE OR REPLACE AGGREGATE
olddb_alarmserver.most_specific_path(INTEGER[]) (
    SFUNC = olddb_alarmserver.group_path_agg_state,
    STYPE = INTEGER[],
    INITCOND = '{}'
);


-- Determine an suitable parent group for kunden-entries using the
-- information in gruppe2alarm.
WITH RECURSIVE
     -- get all pairs of group and ancestor recursively.
     groups_rec(group_id, depth, ancestor_id)
     AS (SELECT id, 0, id FROM users.groups
          UNION ALL
         SELECT r.group_id, r.depth + 1, g.parent
           FROM groups_rec r
           JOIN users.groups g ON r.ancestor_id = g.id
          WHERE g.parent IS NOT NULL)

     -- turn the pairs of groups and ancestors into a mapping of groups
     -- to ancestor paths. The ancestor paths include the groups
     -- themselves as the first element.
   , group_ancestors(group_id, ancestors)
     AS (SELECT g.group_id, 
                array_agg(g.ancestor_id ORDER BY depth) as ancestors
           FROM groups_rec g
       GROUP BY g.group_id)

     -- Determine all pairs of kunden_id and group_path for each entry
     -- in gruppe2alarm where the kunde exists and the group exists and
     -- the group has actually been migrated (the last indirectly
     -- through group_ancestors which was built from the migrated
     -- groups)
   , kunden_group_paths(kunden_id, group_path)
     AS (SELECT k.id, a.ancestors
           FROM olddb_alarmserver.kunden k
           JOIN olddb_benutzer.gruppe2alarm g ON g.slavekunde = k.id
           JOIN group_ancestors a ON g.mastergruppe = a.group_id
          WHERE EXISTS (SELECT * FROM olddb_benutzer.gruppen og
                        WHERE og.id = g.mastergruppe))

     -- Determine the most specific group for each kunde in
     -- kunden_group_paths. all_paths is there mainly for debugging
     -- purposes.
   , kunden_group(kunden_id, most_specific, all_paths)
     AS (SELECT kunden_id, 
                olddb_alarmserver.most_specific_path(group_path),
                json_agg(to_json(group_path))
           FROM kunden_group_paths
       GROUP BY kunden_id)
-- Set parent ID to the most specific group for all customers in
-- kunden_group. Customers not in kunden_group keep NULL as parent_id.
UPDATE olddb_alarmserver.kunde_id_mapping
   SET parent_id = (SELECT most_specific[1]
                      FROM kunden_group
                     WHERE kunde_id = kunden_id);


-- Determine parents for customers for which we don't have one yet.
-- Currently this means that the above code could not get a unique
-- result from gruppe2alarm or no result at all because the customers
-- were not metioned in gruppe2alarm in the first place. We use the new
-- "Customers" group as the parent for those.
--
-- TODO: We may want to treat customers that were not in gruppe2alarm
-- differently from ones whose entries in gruppe2alarm were ambiguous.
UPDATE olddb_alarmserver.kunde_id_mapping
   SET parent_id = olddb_alarmserver.customer_group()
 WHERE parent_id IS NULL;


-- Transfer "kunden" entries

-- We perform this step multiple times to resolve name collisions (some
-- kunden-Names were used up to 3 times) by appending suffixes just as
-- for the group migration.
INSERT INTO users.groups (id, parent, name, role_id)
     SELECT olddb_alarmserver.group_from_kunde(id) as id,
            olddb_alarmserver.parent_from_kunde(id) as parent,
            kunde as name,
            olddb_alarmserver.customer_role()
       FROM olddb_alarmserver.kunden
ON CONFLICT DO NOTHING;

INSERT INTO users.groups (id, parent, name, role_id)
     SELECT olddb_alarmserver.group_from_kunde(id) as id,
            olddb_alarmserver.parent_from_kunde(id) as parent,
            kunde || '(2)' as name,
            olddb_alarmserver.customer_role()
       FROM olddb_alarmserver.kunden k
      WHERE NOT EXISTS (SELECT * FROM users.groups
                         WHERE id = olddb_alarmserver.group_from_kunde(k.id))
ON CONFLICT DO NOTHING;

INSERT INTO users.groups (id, parent, name, role_id)
     SELECT olddb_alarmserver.group_from_kunde(id) as id,
            olddb_alarmserver.parent_from_kunde(id) as parent,
            kunde || '(3)' as name,
            olddb_alarmserver.customer_role()
       FROM olddb_alarmserver.kunden k
      WHERE NOT EXISTS (SELECT * FROM users.groups
                         WHERE id = olddb_alarmserver.group_from_kunde(k.id));
-- No 'ON CONFLICT DO NOTHING' here to make sure we get an error if this
-- step fails to handle all the missing entries.

--
-- Transfer "teilnehmer" as users.
--

-- Create ID-mapping
--
-- For the IDs we use the same strategy as for the customers.
INSERT INTO olddb_alarmserver.teilnehmer_id_mapping(teilnehmer_id, user_id)
     SELECT id, nextval('users.users_id_seq')
       FROM olddb_alarmserver.teilnehmer;

-- Transfer the teilnehmer entries
--
-- users.users(name) must be unique, but the former "teilnehmer" cannot
-- actuall log in and don't actually need a unique name. We handle that
-- by assigning a random UUID as the unique name.
--
-- There are some tuples in the old database that cannot be transferred
-- because they refer to "kunden" entries that do not exist and where
-- therefore group_from_kunde(kunde) returns NULL, so we ignore those.
--
-- TODO: Verify that it's OK to ignore the entries with non-existing
-- "kunden" references
INSERT INTO users.users (id, group_id, name)
     SELECT olddb_alarmserver.user_from_teilnehmer(id) as id,
            olddb_alarmserver.group_from_kunde(kunde) as group_id,
            gen_random_uuid() as name
       FROM olddb_alarmserver.teilnehmer
      WHERE olddb_alarmserver.group_from_kunde(kunde) IS NOT NULL;


--
-- Transfer alarm settings
--

-- The transfer of the teilnehmer entries may have ignored some entries,
-- so we need to make sure to only transfer alarm settings for which a
-- user actually exists.

INSERT INTO users.alarm_settings (id, sms, email, phone, fax, mqtt,
                                  start_date, stop_date, start_time, stop_time,
                                  time_zone, mon, tue, wed, thu, fri, sat, sun,
                                  only_log, stroke_protocol, stroke_as_csv)
     SELECT olddb_alarmserver.user_from_teilnehmer(id),
            sms, email, tel as phone, fax, mqtt,
            olddb_benutzer.convert_time_stamp(startdatum, utc) as start_date,
            olddb_benutzer.convert_time_stamp(stopdatum, utc) as stop_date,
            startzeit as start_time, stopzeit as stop_time,
            CASE WHEN utc THEN 'UTC' ELSE 'CET' END as time_zone,
            mo as mon, di as tue, mi as wed, don as thu, fr as fri, sa as sat,
            so as sun,
            nurlog as only_log, blitzprotokoll as stroke_protocol,
            blitzascsv as stroke_as_csv
       FROM olddb_alarmserver.teilnehmer t
      WHERE EXISTS (SELECT * FROM users.users u
                    WHERE u.id = olddb_alarmserver.user_from_teilnehmer(t.id));


--
-- Transfer alarm templates
--
-- Each of the tables users.alarm_templates_mail,
-- users.alarm_templates_sms, users.alarm_templates_mqtt is filled with
-- the templates taken from the teilnehmer table depending on which of
-- the columns sms, email and/or mqtt are NOT NULL in teilnehmer.
--
-- We re-use the user_ids for the ids of the template tuples.
--
-- TODO: Check that the mqtt part actually works. The old database
-- doesn't have an teilnehmer tuples where mqtt IS NOT NULL.

INSERT INTO users.alarm_templates_sms (id, alarm_subject, allclear_subject,
                                       alarm_text, allclear_text)
     SELECT olddb_alarmserver.user_from_teilnehmer(id),
            betreffalarm as alarm_subject,
            betreffentwarn as allclear_subject,
            alarmierung as alarm_text,
            entwarnung as allclear_text
       FROM olddb_alarmserver.teilnehmer t
      WHERE sms IS NOT NULL
        AND EXISTS (SELECT * FROM users.users u
                    WHERE u.id = olddb_alarmserver.user_from_teilnehmer(t.id));

-- Make sure inserts without explict IDs will not lead to conflicts
SELECT setval('users.alarm_templates_sms_id_seq',
              (SELECT MAX(id) FROM users.alarm_templates_sms));


INSERT INTO users.alarm_templates_mail (id, alarm_subject, allclear_subject,
                                        alarm_text, allclear_text)
     SELECT olddb_alarmserver.user_from_teilnehmer(id),
            betreffalarm as alarm_subject,
            betreffentwarn as allclear_subject,
            alarmierung as alarm_text,
            entwarnung as allclear_text
       FROM olddb_alarmserver.teilnehmer t
      WHERE email IS NOT NULL
        AND EXISTS (SELECT * FROM users.users u
                    WHERE u.id = olddb_alarmserver.user_from_teilnehmer(t.id));

-- Make sure inserts without explict IDs will not lead to conflicts
SELECT setval('users.alarm_templates_mail_id_seq',
              (SELECT MAX(id) FROM users.alarm_templates_mail));

INSERT INTO users.alarm_templates_mqtt (id, alarm_subject, allclear_subject,
                                        alarm_text, allclear_text)
     SELECT olddb_alarmserver.user_from_teilnehmer(id),
            betreffalarm as alarm_subject,
            betreffentwarn as allclear_subject,
            alarmierung as alarm_text,
            entwarnung as allclear_text
       FROM olddb_alarmserver.teilnehmer t
      WHERE mqtt IS NOT NULL
        AND EXISTS (SELECT * FROM users.users u
                    WHERE u.id = olddb_alarmserver.user_from_teilnehmer(t.id));

-- Make sure inserts without explict IDs will not lead to conflicts
SELECT setval('users.alarm_templates_mqtt_id_seq',
              (SELECT MAX(id) FROM users.alarm_templates_mqtt));


INSERT INTO users.alarm_templates_fax (id, alarm_subject, allclear_subject,
                                       alarm_text, allclear_text)
     SELECT olddb_alarmserver.user_from_teilnehmer(id),
            betreffalarm as alarm_subject,
            betreffentwarn as allclear_subject,
            alarmierung as alarm_text,
            entwarnung as allclear_text
       FROM olddb_alarmserver.teilnehmer t
      WHERE fax IS NOT NULL
        AND EXISTS (SELECT * FROM users.users u
                    WHERE u.id = olddb_alarmserver.user_from_teilnehmer(t.id));

-- Make sure inserts without explict IDs will not lead to conflicts
SELECT setval('users.alarm_templates_fax_id_seq',
              (SELECT MAX(id) FROM users.alarm_templates_fax));


UPDATE users.alarm_settings s
   SET alarm_template_sms_id = (SELECT id FROM users.alarm_templates_sms t
                                          WHERE t.id = s.id),
       alarm_template_mail_id = (SELECT id FROM users.alarm_templates_mail t
                                          WHERE t.id = s.id),
       alarm_template_mqtt_id = (SELECT id FROM users.alarm_templates_mqtt t
                                          WHERE t.id = s.id),
       alarm_template_fax_id = (SELECT id FROM users.alarm_templates_fax t
                                         WHERE t.id = s.id);

--
-- Transfer map layers
--

-- We keep the IDs from the old database.
--
-- We set name and layer_type to empty strings because the must be NOT
-- NULL and there doesn't seem to be an obvious way to assign other
-- values (although perhaps one could re-use geoserver_name as the name).
--
-- We ignore externer_parameter because it's JSON in the schema and
-- there's no obvious way in which to convert the strings from the old
-- database.
--
-- TODO: Better values for name and layer_type
--
-- TODO: Migrate externer_parameter.

INSERT INTO users.map_layers (id, geoserver_name, name, reload,
                              baselayer, layer_type,
                              single_tile,
                              default_name,
                              external_server)
     SELECT id, geoserver_ebene as geoserver_name, standardname as name, reload,
            baselayer, '' as layer_type,
            singletile as single_tile,
            standardname as default_name,
            externer_server as external_server
       FROM olddb_benutzer.ebenen;
-- Make sure inserts without explict IDs will not lead to conflicts
SELECT setval('users.map_layers_id_seq',
              (SELECT MAX(id) FROM users.map_layers));


--
-- Transfer group map layers
--

-- We keep the IDs from the old database.
--
-- In the old database gruppen_ebenen there are some entries which refer
-- to non-existent layers. We ignore those in the migration because they
-- couldn't have been used by the application.
--
-- Also, some gruppen_ebenen reference groups that have not been
-- transferred because no role could be determined for them. We omit
-- those gruppen_ebenen entries.
--
-- TODO: verify that it's OK to omit gruppen_ebenen entries which
-- reference groups that have not been transferred.
INSERT INTO users.group_map_layer (id, group_id, map_layer_id, reload_time,
                                   display_name, feature_info, checked_on_login,
                                   permanent, cql_filter, popup_template)
     SELECT id, gruppen_id, ebenen_id, reload_time,
            anzeige_name as display_name, feature_info, checked_onlogin,
            permanent, cql_filter, popup_template
       FROM olddb_benutzer.gruppen_ebenen
      WHERE EXISTS (SELECT * FROM users.map_layers WHERE id = ebenen_id)
        AND EXISTS (SELECT * FROM users.groups WHERE id = gruppen_id);
-- Make sure inserts without explict IDs will not lead to conflicts
SELECT setval('users.group_map_layer_id_seq',
              (SELECT MAX(id) FROM users.group_map_layer));


--
-- Migrate areas and area units
--
-- Strategy:
--  1. copy alarmserver.gebiete into alarm.alarm_areas
--  2. create alarm.alarm_area_unit entries for the new alarm_areas
--  3. copy alarmserver.gebiet2gruppe and alarmserver.gebietgruppen into
--     alarm.alarm_area_unit


-- 1. copy alarmserver.gebiete into alarm.alarm_areas
-- We re-use the gebiet IDs as alarm_areas IDs.
--
-- Some gebiet entries refer to non-exiting kunde entries. We ignore
-- those.
--
-- Some very few gebiet entries have NULL as gebietgeom. We ignore
-- those, because they couldn't have been used for actual notifications
-- and their names suggest that they've been created for tests.
--
-- The columns 'letzterblitz', 'blitzanzahl' and 'alarm' from the old
-- alarmserver.gebiete need not to be migrated, as these values are
-- highly volatile and only relevant during lightning in the area.
--
-- TODO: Verify that it's OK to ignore entries with non-existing
-- kunde-entries. There might be ways to avoid this using the kunde
-- column of gebietgruppen.

INSERT INTO alarm.alarm_areas (id, geom, alarm_duration, radius, center,
                               icon, area, inner_radius, stroke_threshold)
     SELECT id as id,
            gebietgeom as geom,
            alarmdauer as alarm_duration,
            radius,
            zentrum as center,
            icon,
            flaeche as area,
            innerradius as inner_radius,
            blitzezumalarm as stroke_threshold
       FROM olddb_alarmserver.gebiete
      WHERE gebietgeom IS NOT NULL
        AND olddb_alarmserver.group_from_kunde(kunde) IS NOT NULL;
-- Make sure inserts without explict IDs will not lead to conflicts
SELECT setval('alarm.alarm_areas_id_seq',
              (SELECT MAX(id) FROM alarm.alarm_areas));


-- 2. create alarm.alarm_area_unit entries for the new alarm_areas For
-- simplicity, re-use the alarm_areas/gebiete IDs. We make use of this
-- fact when transferring the alarmserver.alarme below.
--
-- The alarm_area_unit entries created in this step are *not* extended,
-- so we assign false to is_extended for all of them.
--
-- Due to name collisions we do this twice, appending the radius to the
-- name in the second attempt to make the names unique.
INSERT INTO alarm.alarm_area_unit (id, name, group_id, is_extended)
     SELECT id, gebiet as name,
            olddb_alarmserver.group_from_kunde(kunde) as group_id,
            false as is_extended
       FROM olddb_alarmserver.gebiete g
      WHERE EXISTS (SELECT * FROM alarm.alarm_areas a WHERE a.id = g.id)
ON CONFLICT DO NOTHING;

INSERT INTO alarm.alarm_area_unit (id, name, group_id)
     SELECT id, gebiet || ' ' || radius as name,
            olddb_alarmserver.group_from_kunde(kunde) as group_id
       FROM olddb_alarmserver.gebiete g
      WHERE EXISTS (SELECT * FROM alarm.alarm_areas a WHERE a.id = g.id)
        AND NOT EXISTS (SELECT * FROM alarm.alarm_area_unit u
                        WHERE u.id = g.id);
-- Make sure inserts without explict IDs will not lead to conflicts
SELECT setval('alarm.alarm_area_unit_id_seq',
              (SELECT MAX(id) FROM alarm.alarm_area_unit));

-- Link alarm_area_unit and alarm_are entries
INSERT INTO alarm.alarm_area_map (alarm_area_id, alarm_area_unit_id)
     SELECT id, id
       FROM alarm.alarm_area_unit;

-- 3. copy alarmserver.gebiet2gruppe and alarmserver.gebietgruppen into
--    alarm.alarm_area_unit

-- In the old database, there's one gebietgruppen entry with a
-- non-existing kunde. We ignore that one.
--
-- The alarm_area_unit entries created in this step *are* extended, so
-- we set is_extended to true for all of the entries created here..
--
-- When filling alarm_area_map we need to ignore both the gebietgruppen
-- entries we ignored before and the gebiete entries we ignored when
-- filling alarm_areas.
--
-- TODO: Verify that it's OK to ignore gebietgruppen entries with
-- non-existing kunde-entries
--
-- TODO: Once we've finalized which entries to ignore: Verify that we
-- correctly replicate the rules when filling alarm_area_map.

INSERT INTO alarm.alarm_area_unit (name, group_id, is_extended)
     SELECT gg.gruppenname as name,
            olddb_alarmserver.group_from_kunde(gg.kunde) as group_id,
            true as is_extended
      FROM olddb_alarmserver.gebietgruppen gg
     WHERE olddb_alarmserver.group_from_kunde(gg.kunde) IS NOT NULL;

INSERT INTO alarm.alarm_area_map (alarm_area_id, alarm_area_unit_id)
     SELECT g.id,
            (SELECT id FROM alarm.alarm_area_unit
              WHERE name = gg.gruppenname
                AND group_id = olddb_alarmserver.group_from_kunde(gg.kunde))
       FROM olddb_alarmserver.gebietgruppen gg
       JOIN olddb_alarmserver.gebiet2gruppe g2g ON g2g.gruppe = gg.id
       JOIN olddb_alarmserver.gebiete g ON g.id = g2g.gebiet
      WHERE olddb_alarmserver.group_from_kunde(gg.kunde) IS NOT NULL
        AND olddb_alarmserver.group_from_kunde(g.kunde) IS NOT NULL
        AND g.gebietgeom IS NOT NULL;



-- Migrate alarms
--
-- We re-use the ids of olddb_alarmserver.alarme as the ids of
-- alam.alarms. We rely on the fact that the ids of the "gebiete" in the
-- old database have been used as the ids of the corresponding
-- alarm_area in the new database and that the alarm_area_unit for that
-- alarm_area also has the same id value.

INSERT INTO alarm.alarms (id, alarm_area_unit_id, user_id)
     SELECT id, gebiet as alarm_area_unit_id,
            olddb_alarmserver.user_from_teilnehmer(teilnehmer) as user_id
       FROM olddb_alarmserver.alarme;

-- Make sure inserts without explict IDs will not lead to conflicts
SELECT setval('alarm.alarms_id_seq', (SELECT MAX(id) FROM alarm.alarms));


-- Migrate notifications
--
-- We re-use the ids of olddb_alarmserver.alarme as the ids of
-- alam.alarms.
--
-- For the area value which references alarm.alarm_areas we use the
-- value the corresponding alarm.alarms tuple uses as
-- alarm_area_unit_id. Due to the way we re-use IDs for areas and
-- area-units these value are actually the same for the notification we
-- migrate from the old database.
--
-- There's one alarm whose alarm_area_unit_id is NULL. We ignore
-- notifications that reference such alarms.
--
-- There are some notifications in the old database that reference
-- alarms that don't exist. We ignore those.
--
-- TODO: Should we just not migrate the alarm whose alarm_area_unit_id
-- is NULL instead of ignoring notifications that reference it?

INSERT INTO alarm.notifications (id, alarm, area, date,
                                 done, type, status, status_info)
     SELECT m.id, m.alarm, a.alarm_area_unit_id, m.eintrag as date,
            m.erledigt as done,
            olddb_alarmserver.convert_alarmtyp(typ) as type,
            olddb_alarmserver.convert_send_state(status), status_info
       FROM olddb_alarmserver.meldungen m
       JOIN alarm.alarms a ON a.id = m.alarm
      WHERE a.alarm_area_unit_id IS NOT NULL;

-- Make sure inserts without explict IDs will not lead to conflicts
SELECT setval('alarm.notifications_id_seq',
              (SELECT MAX(id) FROM alarm.notifications));

--
-- Step 3: Cleanup
--
-- This section drops the schemas, extensions and other things created
-- during Step 1.

DROP SCHEMA olddb_benutzer CASCADE;
DROP SCHEMA olddb_alarmserver CASCADE;
DROP SCHEMA alarmserver CASCADE;

DROP SERVER olddb CASCADE;

DROP EXTENSION pgcrypto;
DROP EXTENSION postgres_fdw;
