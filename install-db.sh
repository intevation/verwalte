#!/bin/bash -e

ME=`basename "$0"`
BASEDIR=`dirname "$0"`

usage()
{
  cat <<EOF
$ME [OPTION]...

Options:
  -d, --db=NAME    create the database NAME.  Default: "blids"
  -e               use existing database Default: "blids"
  -s               install schema and data Default: 1
  -p, --port=PORT  connect do the postgresql cluster at PORT.
                   Default is the postgresql standard port 5432
  -c, --cron       setup pg_cron configuration
      --drop       drop database
      --demo       setup with demo data
      --help       display this help and exit

EOF
}

# Defaults:

db=blids
port=5432
cron=0
drop=0
demo=0
exists=0
schema=1

# Parse options:

OPTS=`getopt \
      -l help,db:,port:,cron,drop,demo, \
      -o d:p:s:c,e -n "$ME" -- "$@"`
[ $? -eq 0 ] || { usage ; exit 1 ; }

eval set -- "$OPTS"

while true ; do
  case "$1" in
    --db|-d)
      db="$2"
      shift 2
      ;;
    -e)
      exists=1
      shift 1
      ;;
    -s)
      schema="$2"
      shift 2
      ;;
    --port|-p)
      port="$2"
      shift 2
      ;;
    --cron|-c)
      cron=1
      shift 1
      ;;
    --drop)
      drop=1
      shift 1
      ;;
    --demo)
      demo=1
      shift 1
      ;;
    --help)
      { usage ; exit 0 ; }
      ;;
    --)
      shift
      break
      ;;
  esac
done



if [[ drop -eq 0 ]] ; then
  # Default operation: create schema
  echo "Create database $db"
  if [[ exists -eq 0 ]] ; then
      createdb -p "$port" "$db"
  fi
  if [[ schema -eq 1 ]] ; then
      psql -qtv ON_ERROR_STOP= -p "$port" -d "$db" \
           -c "SET client_min_messages TO WARNING;" \
           -f "$BASEDIR/db-schema.sql" \
           -f "$BASEDIR/logging.sql" \
           -f "$BASEDIR/schema-data.sql" \
           -f "$BASEDIR/roles.sql" \
           -f "$BASEDIR/role-blitz.sql"
      for d in "$BASEDIR"/updates/* ; do
        new_ver=$( basename $d )
      done
      psql -qtv ON_ERROR_STOP= -p "$port" -d "$db" --command \
        "INSERT INTO blids_schema_version(version) VALUES ($new_ver)"

  fi
  if [[ cron -eq 1 ]] ; then
    psql -qtv ON_ERROR_STOP= -p "$port" -d "$db" \
      -c "SET client_min_messages TO WARNING;" \
      -f "$BASEDIR/pg-cron.sql"
  fi
  if [[ demo -eq 1 ]] ; then
    psql -qtv ON_ERROR_STOP= -p "$port" -d "$db" \
      -c "SET client_min_messages TO WARNING;" \
      -f "$BASEDIR/demo-data.sql"
  fi
else
  # Evil mode: drop everything
  echo "Really drop database '$db'? [type 'yes']: "
  read a
  if [[ $a == "yes" ]] ; then
    dropdb --if-exists -p "$port" "$db"
  else
    echo "No harm done."
  fi
fi
