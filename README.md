# verwalte - database schema for the blids universe

- [verwalte - database schema for the blids universe](#verwalte---database-schema-for-the-blids-universe)
  - [Setup](#setup)
  - [Migrate data from old database](#migrate-data-from-old-database)
  - [Import migrated data to new database](#import-migrated-data-to-new-database)
  - [Create a visualization of the database schema](#create-a-visualization-of-the-database-schema)

## Setup

Clone this Repository and use the `install-db.sh` script to setup the
database.

The script takes arguments for database name and port. Use `--help` for
usage info.

## Migrate data from old database

```shell
psql -d newdb --set=olddbname='OLDDB' [ --set=superuser='ROLE' ] -f migrate-db.sql
```

## Import migrated data to new database

```shell
pg_dump -f blids-migrated-20200316.dump $DB
```

Use the `after-migration.sql` to setup the following:

- The roles and permissions concept
- Empty profiles for all users
- Host-Theme (mapping to the host must be adjusted accordingly)
- Themes (4 example themes are then in the DB, default for all is 1.)

Further TODOs for `after-migration.sql`:

- external_parameter
- layer_type
- ...

## Create a visualization of the database schema

Create a visualization of the database schema using [PostgreSQL Autodoc
](https://github.com/cbbrowne/autodoc) and [Graphviz](https://www.graphviz.org/).

```shell
postgresql_autodoc -p $DB_PORT -h $DB_HOST \
                   -d $DB_NAME -U $USER  \
                   -t dot -l $PATH_TO_TEMPLATES

dot -Tpdf blids.dot > blids.pdf
```
