-- Demo data with users, configured areas throughout germany, and a set of
-- randomly inserted strokes

SET statement_timeout = 0;
SET lock_timeout = 0;
SET idle_in_transaction_session_timeout = 0;
SET client_encoding = 'UTF8';
SET standard_conforming_strings = on;
SET check_function_bodies = false;
SET xmloption = content;
SET client_min_messages = warning;
SET row_security = off;

-- user/role definitions

INSERT INTO users.roles (id, parent, name) VALUES
  (1, NULL, 'Superadmin'),
  (2, 1, 'Lightningadmin'),
  (3, 1, 'Alarmadmin'),
  (4, 3, 'Alarmuser'),
  (5, 2, 'Lightninguser'),
  (6, 4, 'Alarmonlyuser'),
  (7,5, 'Lightningonlyuser');

SELECT setval('users.roles_id_seq', (SELECT max(id) FROM users.roles), true);

INSERT INTO users.themes (id, name, data) VALUES
  (1, 'generic', '{
    "theme":{
      "dark": false,
      "themes": {
        "light": {
          "primary": {
            "base": "#afafaf",
            "darken1": "#008888",
            "lighten1": "#a0dcdc"
          },
          "secondary": "#8585F4",
          "accent": "#82B1FF",
          "error": "#444444",
          "info": "#33b5e5",
          "success": "#00C851",
          "warning": "#ffbb33"
        },
        "dark": {
          "primary": {
            "base": "#009999",
            "darken1": "#008888",
            "lighten1": "#a0dcdc"
          },
          "secondary": "#8585F4",
          "accent": "#82B1FF",
          "error": "#444444",
          "info": "#33b5e5",
          "success": "#00C851",
          "warning": "#ffbb33"
        }
      }
    }
  }'),
  (2, 'Admin-Theme', '{
  "theme":{
    "dark": false,
    "themes": {
      "light": {
        "primary": {
          "base": "#afafaf",
          "darken1": "#008888",
          "lighten1": "#a0dcdc"
        },
        "secondary": "#8585F4",
        "accent": "#82B1FF",
        "error": "#444444",
        "info": "#33b5e5",
        "success": "#00C851",
        "warning": "#ffbb33"
      },
      "dark": {
        "primary": {
          "base": "#009999",
          "darken1": "#008888",
          "lighten1": "#a0dcdc"
        },
        "secondary": "#8585F4",
        "accent": "#82B1FF",
        "error": "#444444",
        "info": "#33b5e5",
        "success": "#00C851",
        "warning": "#ffbb33"
      }
    }
  }
  }'),
  (3, 'Theme For Western Users', '{
  "theme":{
    "dark": true,
    "themes": {
      "light": {
        "primary": {
          "base": "#afafff",
          "darken1": "#0088f8",
          "lighten1": "#a0dcfc"
        },
        "secondary": "#8f85F4",
        "accent": "#8fB1FF",
        "error": "#4f4444",
        "info": "#3fb5e5",
        "success": "#0fC851",
        "warning": "#f0bb33"
      },
      "dark": {
        "primary": {
          "base": "#009999",
          "darken1": "#008888",
          "lighten1": "#a0dcdc"
        },
        "secondary": "#8585F4",
        "accent": "#82B1FF",
        "error": "#444444",
        "info": "#33b5e5",
        "success": "#00C851",
        "warning": "#ffbb33"
      }
    }
  }
  }'),
  (4, 'Theme For Eastern Users', '{
  "theme":{
    "dark": false,
    "themes": {
      "light": {
        "primary": {
          "base": "#ffafaf",
          "darken1": "#a08888",
          "lighten1": "#aadcdc"
        },
        "secondary": "#8585F4",
        "accent": "#82B1FF",
        "error": "#444444",
        "info": "#33b5e5",
        "success": "#00C851",
        "warning": "#ffbb33"
      },
      "dark": {
        "primary": {
          "base": "#009999",
          "darken1": "#008888",
          "lighten1": "#a0dcdc"
        },
        "secondary": "#8585F4",
        "accent": "#82B1FF",
        "error": "#444444",
        "info": "#33b5e5",
        "success": "#00C851",
        "warning": "#ffbb33"
      }
    }
  }
  }');

INSERT INTO blitz.data_areas (id, geom) VALUES
  (1, ST_GeomFromText('POLYGON ((5.99625 52.00125,7.9875 52.005,8.0025 50.00625,6.0 49.99125,5.99625 52.00125))', 4326)),
  (2, ST_GeomFromText('POLYGON ((5.61375 53.98875,13.98375 53.98875,13.98375 47.95875,5.68875 48.01875,5.61375 53.98875))', 4326)),
  (3, ST_GeomFromText('POLYGON ((8.604375 51.9354910714286,8.7811607142857 51.9947544642857,8.92731026785713 51.9209263392857,8.93635044642856 51.7401227678571,8.80376116071427 51.6276227678571,8.67418526785713 51.6642857142857,8.56369419642856 51.7446428571428,8.51949776785713 51.8029017857143,8.604375 51.9354910714286))', 4326)),
  (4, ST_GeomFromText('POLYGON ((9.63445312499998 53.7294642857143,9.61838169642856 48.1446428571428,11.1371316964286 48.0803571428571,11.1933816964286 53.7214285714286,9.63445312499998 53.7294642857143))', 4326));

SELECT setval('blitz.data_areas_id_seq', (SELECT max(id) FROM blitz.data_areas), true);

INSERT INTO users.alarm_config (id, config) VALUES
  (1, '{}'),
  (2, '{}'),
  (3, '{}');

SELECT setval('users.alarm_config_id_seq', (SELECT max(id) FROM users.alarm_config), true);

INSERT INTO users.groups VALUES
  (DEFAULT, NULL, 'Superadmin', 1, 2, 999999999, NULL, NULL, false);

INSERT INTO users.groups VALUES
  (DEFAULT, (SELECT id from users.groups WHERE name='Superadmin'), 'Alarm-Admin', 2, 2, 999999999, NULL, NULL, false),
  (DEFAULT, (SELECT id from users.groups WHERE name='Superadmin'), 'Blitzadmin', 3, 2, 999999999, NULL, NULL, false);

INSERT INTO users.groups VALUES
  (DEFAULT, (SELECT id from users.groups WHERE name='Alarm-Admin'), 'Alarme West', 4, 3, DEFAULT, 1, NULL, false),
  (DEFAULT, (SELECT id from users.groups WHERE name='Blitzadmin'), 'Blitze West', 5, 3, DEFAULT, 1, NULL, false),
  (DEFAULT, (SELECT id from users.groups WHERE name='Alarm-Admin'), 'Alarme Ost', 4, 4, DEFAULT, 1, NULL, false),
  (DEFAULT, (SELECT id from users.groups WHERE name='Blitzadmin'), 'Blitze Ost', 5, 4, DEFAULT, 1, NULL, false),
  (DEFAULT, (SELECT id from users.groups WHERE name='Alarm-Admin'), 'Alarm-Admin Paderborn', 2, 1, DEFAULT, 1, NULL, false),
  (DEFAULT, (SELECT id from users.groups WHERE name='Blitzadmin'), 'Blitzadmin Paderborn', 3, 1, DEFAULT, 1, NULL, false);

INSERT INTO users.groups VALUES
  (DEFAULT, (SELECT id from users.groups WHERE name='Blitze West'), 'Blitzanzeige (Westen)', 7, 3, 0, 1, NULL, false),
  (DEFAULT, (SELECT id from users.groups WHERE name='Blitze Ost'), 'Blitzanzeige (Osten)', 7, 4, 0, 1, NULL, false),
  (DEFAULT, (SELECT id from users.groups WHERE name='Alarm-Admin Paderborn'), 'Blitzanzeige (Gesamt)', 7, 1, 0, 1, NULL, false),
  (DEFAULT, (SELECT id from users.groups WHERE name='Alarm-Admin Paderborn'), 'Blitzanzeige (Paderborn)', 7, 1, 0, 1, NULL, false);

-- TODO: still the same as data_areas
INSERT INTO blitz.stroke_areas (id, group_id, geom) VALUES
  (1, 12, ST_GeomFromText('POLYGON ((5.99625 52.00125,7.9875 52.005,8.0025 50.00625,6.0 49.99125,5.99625 52.00125))', 4326)),
  (2, 1, ST_GeomFromText('POLYGON ((5.61375 53.98875,13.98375 53.98875,13.98375 47.95875,5.61375 47.95875,5.61375 53.98875))', 4326)),
  (3, 2, ST_GeomFromText('POLYGON ((5.61375 53.98875,13.98375 53.98875,13.98375 47.95875,5.61375 47.95875,5.61375 53.98875))', 4326)),
  (4, 3, ST_GeomFromText('POLYGON ((5.61375 53.98875,13.98375 53.98875,13.98375 47.95875,5.61375 47.95875,5.61375 53.98875))', 4326)),
  (5, 4, ST_GeomFromText('POLYGON ((5.61375 53.98875,13.98375 53.98875,13.98375 47.95875,5.61375 47.95875,5.61375 53.98875))', 4326)),
  (6, 5, ST_GeomFromText('POLYGON ((5.61375 53.98875,13.98375 53.98875,13.98375 47.95875,5.61375 47.95875,5.61375 53.98875))', 4326)),
  (7, 10, ST_GeomFromText('POLYGON ((5.61375 53.98875,13.98375 53.98875,13.98375 47.95875,5.61375 47.95875,5.61375 53.98875))', 4326)),
  (8, 8, ST_GeomFromText('POLYGON ((8.604375 51.9354910714286,8.7811607142857 51.9947544642857,8.92731026785713 51.9209263392857,8.93635044642856 51.7401227678571,8.80376116071427 51.6276227678571,8.67418526785713 51.6642857142857,8.56369419642856 51.7446428571428,8.51949776785713 51.8029017857143,8.604375 51.9354910714286))', 4326)),
  (9, 9, ST_GeomFromText('POLYGON ((8.604375 51.9354910714286,8.7811607142857 51.9947544642857,8.92731026785713 51.9209263392857,8.93635044642856 51.7401227678571,8.80376116071427 51.6276227678571,8.67418526785713 51.6642857142857,8.56369419642856 51.7446428571428,8.51949776785713 51.8029017857143,8.604375 51.9354910714286))', 4326)),
  (10, 13, ST_GeomFromText('POLYGON ((8.604375 51.9354910714286,8.7811607142857 51.9947544642857,8.92731026785713 51.9209263392857,8.93635044642856 51.7401227678571,8.80376116071427 51.6276227678571,8.67418526785713 51.6642857142857,8.56369419642856 51.7446428571428,8.51949776785713 51.8029017857143,8.604375 51.9354910714286))', 4326)),
  (11, 6, ST_GeomFromText('POLYGON ((9.63445312499998 53.7294642857143,9.61838169642856 48.1446428571428,11.1371316964286 48.0803571428571,11.1933816964286 53.7214285714286,9.63445312499998 53.7294642857143))', 4326)),
  (12, 7, ST_GeomFromText('POLYGON ((9.63445312499998 53.7294642857143,9.61838169642856 48.1446428571428,11.1371316964286 48.0803571428571,11.1933816964286 53.7214285714286,9.63445312499998 53.7294642857143))', 4326)),
  (13, 11, ST_GeomFromText('POLYGON ((9.63445312499998 53.7294642857143,9.61838169642856 48.1446428571428,11.1371316964286 48.0803571428571,11.1933816964286 53.7214285714286,9.63445312499998 53.7294642857143))', 4326));

SELECT setval('blitz.stroke_areas_id_seq', (SELECT max(id) FROM blitz.stroke_areas), true);

INSERT INTO users.app_settings (group_id, max_display_time, archive_days, archive_since, sound, max_alarm_users, max_alarm_areas) VALUES
  ((SELECT id FROM users.groups WHERE name='Superadmin'), 10, 365, TIMESTAMP '2001-01-01 00:00:00+00', TRUE, 100, 100),
  ((SELECT id FROM users.groups WHERE name='Alarm-Admin'), 10, 365, TIMESTAMP '2010-01-01 00:00:00+00', TRUE, 100, 100),
  ((SELECT id FROM users.groups WHERE name='Blitzadmin'), 10, 100, TIMESTAMP '2010-01-01 00:00:00+00', TRUE, DEFAULT, DEFAULT),
  ((SELECT id FROM users.groups WHERE name='Alarme West'), 10, 100, TIMESTAMP '2020-01-01 00:00:00+00',TRUE,DEFAULT, DEFAULT),
  ((SELECT id FROM users.groups WHERE name='Blitze West'), 10, 100, TIMESTAMP '2020-01-01 00:00:00+00',TRUE,100,100),
  ((SELECT id FROM users.groups WHERE name='Blitze Ost'), 10, 100, TIMESTAMP '2020-01-01 00:00:00+00',TRUE,DEFAULT, DEFAULT),
  ((SELECT id FROM users.groups WHERE name='Alarme Ost'), 10, 100, TIMESTAMP '2020-01-01 00:00:00+00',TRUE,50,50),
  ((SELECT id FROM users.groups WHERE name='Alarm-Admin Paderborn'), 10, 100, TIMESTAMP '2020-01-01 00:00:00+00',TRUE,DEFAULT, DEFAULT),
  ((SELECT id FROM users.groups WHERE name='Blitzadmin Paderborn'), 10, 100, TIMESTAMP '2020-01-01 00:00:00+00',FALSE, 10, 10),
  ((SELECT id FROM users.groups WHERE name='Blitzanzeige (Westen)'), 10, 100, TIMESTAMP '2020-01-01 00:00:00+00',TRUE, 0, 0),
  ((SELECT id FROM users.groups WHERE name='Blitzanzeige (Osten)'), 10, 100, TIMESTAMP '2020-01-01 00:00:00+00',TRUE,0, 0),
  ((SELECT id FROM users.groups WHERE name='Blitzanzeige (Gesamt)'), 10, 100, TIMESTAMP '2020-01-01 00:00:00+00',TRUE,0, 0),
  ((SELECT id FROM users.groups WHERE name='Blitzanzeige (Paderborn)'), 10, 100, TIMESTAMP '2020-01-01 00:00:00+00',FALSE, 0, 0);

INSERT INTO users.users (name, password, group_id, start_date, stop_date, first_name, last_name) VALUES
  ('monty', MD5('mo2Nty3'), (SELECT id FROM users.groups WHERE name = 'Superadmin'), DEFAULT, DEFAULT,'Montgomery', 'Burns'),
  ('quimby', MD5('ma1YoR!'), (SELECT id FROM users.groups WHERE name = 'Superadmin'), DEFAULT, DEFAULT,'Joseph', 'Quimby'),
  ('homer', MD5('ho2Mer3'), (SELECT id FROM users.groups WHERE name = 'Alarme West'), DEFAULT, DEFAULT,'Homer', 'Simpson'),
  ('lisa', MD5('li2Sa2'), (SELECT id FROM users.groups WHERE name = 'Alarm-Admin'), DEFAULT, DEFAULT,'Lisa', 'Simpson'),
  ('marge', MD5('ma2Rge3'), (SELECT id FROM users.groups WHERE name = 'Blitzadmin'), DEFAULT, DEFAULT,'Marge', 'Simpson'),
  ('bart', MD5('ba2Rt2'), (SELECT id FROM users.groups WHERE name = 'Alarme West'), DEFAULT, DEFAULT,'Bart', 'Simpson'),
  ('maggie', MD5('ma22ggIE'), (SELECT id FROM users.groups WHERE name = 'Blitzanzeige (Westen)'), DEFAULT, DEFAULT,'Maggie', 'Simpson'),
  ('abe', MD5('a1b2e5'), (SELECT id FROM users.groups WHERE name = 'Alarme West'), DEFAULT, DEFAULT,'Abraham Jebediah', 'Simpson'),
  ('patty', MD5('p3at2ty'), (SELECT id FROM users.groups WHERE name = 'Alarme West'), DEFAULT, DEFAULT,'Patty', 'Bouvier'),
  ('selma', MD5('selma1'), (SELECT id FROM users.groups WHERE name = 'Alarme West'), DEFAULT, DEFAULT,'Selma','Bouvier-Terwilliger-Hutz-McClure-Stu-Simpson'),
  ('ned', MD5('ned123'), (SELECT id FROM users.groups WHERE name = 'Alarm-Admin'), DEFAULT, DEFAULT, 'Ned', 'Flanders'),
  ('rod', MD5('rodflanders'), (SELECT id FROM users.groups WHERE name = 'Alarme Ost'), DEFAULT, DEFAULT,'Rod', 'Flanders'),
  ('todd', MD5('toddflanders'), (SELECT id FROM users.groups WHERE name = 'Blitzanzeige (Osten)'), DEFAULT, DEFAULT,'Todd', 'Fladners'),
  ('maude', MD5('maudeflanders'), (SELECT id FROM users.groups WHERE name = 'Blitzanzeige (Osten)'), now() - '1 year'::interval, now() - '3 weeks'::interval,'Maude', 'Flanders'),
  ('krusty', MD5('CloWn'), (SELECT id FROM users.groups WHERE name = 'Alarme West'), DEFAULT, DEFAULT,'Herschel Shmoikel Pinkus Yerucham', 'Krustofski'),
  ('apu', MD5('ap2U1'), (SELECT id FROM users.groups WHERE name = 'Alarme West'), DEFAULT, DEFAULT,'Apu', 'Nahasapeemapetilon'),
  ('moe', MD5('MoE2'), (SELECT id FROM users.groups WHERE name = 'Blitzanzeige (Westen)'), DEFAULT, DEFAULT,'Morris', 'Szyslak'),
  ('timothy', MD5('Re2VeRe2Nd'), (SELECT id FROM users.groups WHERE name = 'Alarm-Admin Paderborn'), DEFAULT, DEFAULT,'Timothy', 'Lovejoy'),
  ('helen', MD5('hel1en'), (SELECT id FROM users.groups WHERE name = 'Blitzanzeige (Paderborn)'), DEFAULT, DEFAULT,'Helen', 'Lovejoy'),
  ('jessica', MD5('jessica'), (SELECT id FROM users.groups WHERE name = 'Blitzanzeige (Paderborn)'), DEFAULT, DEFAULT,'Jessica', 'Lovejoy'),
  ('nelson', MD5('ne2Lson4'), (SELECT id FROM users.groups WHERE name = 'Alarme West'), DEFAULT, DEFAULT,'Nelson', 'Muntz'),
  ('milhouse', MD5('mi2Lhou4Se'), (SELECT id FROM users.groups WHERE name = 'Blitzanzeige (Gesamt)'), DEFAULT, DEFAULT, 'Milhouse', 'van Houten'),
  ('edna', MD5('ed2Na'), (SELECT id FROM users.groups WHERE name = 'Blitzanzeige (Gesamt)'), now() - '2 years'::interval, now() - '5 months'::interval,'Edna', 'Krabappel'),
  ('willie', MD5('wi2Llie4'), (SELECT id FROM users.groups WHERE name = 'Alarme Ost'), DEFAULT, DEFAULT, 'Willie', 'MacMoran'),
  ('barney', MD5('ba2Rney4'), (SELECT id FROM users.groups WHERE name = 'Blitzanzeige (Osten)'), DEFAULT, DEFAULT,'Barney', 'Gumble'),
  ('waylon', MD5('wa2Ylon4'), (SELECT id FROM users.groups WHERE name = 'Blitzanzeige (Osten)'), DEFAULT, DEFAULT,'Waylon', 'Smithers');

INSERT INTO users.user_roles (user_id, role_id) VALUES
  ((SELECT id from users.users WHERE name='monty'), 1),
  ((SELECT id from users.users WHERE name='quimby'), 1),
  ((SELECT id from users.users WHERE name='homer'), 4),
  ((SELECT id from users.users WHERE name='lisa'), 3),
  ((SELECT id from users.users WHERE name='marge'), 2),
  ((SELECT id from users.users WHERE name='bart'), 4),
  ((SELECT id from users.users WHERE name='maggie'), 7),
  ((SELECT id from users.users WHERE name='abe'), 7),
  ((SELECT id from users.users WHERE name='patty'), 4),
  ((SELECT id from users.users WHERE name='selma'), 4),
  ((SELECT id from users.users WHERE name='ned'), 3),
  ((SELECT id from users.users WHERE name='rod'), 4),
  ((SELECT id from users.users WHERE name='todd'), 7),
  ((SELECT id from users.users WHERE name='maude'), 7),
  ((SELECT id from users.users WHERE name='krusty'),4 ),
  ((SELECT id from users.users WHERE name='apu'), 4),
  ((SELECT id from users.users WHERE name='moe'), 7),
  ((SELECT id from users.users WHERE name='timothy'), 3),
  ((SELECT id from users.users WHERE name='helen'), 7),
  ((SELECT id from users.users WHERE name='jessica'), 7),
  ((SELECT id from users.users WHERE name='nelson'), 4),
  ((SELECT id from users.users WHERE name='milhouse'), 7),
  ((SELECT id from users.users WHERE name='edna'), 7),
  ((SELECT id from users.users WHERE name='willie'), 7),
  ((SELECT id from users.users WHERE name='barney'), 7),
  ((SELECT id from users.users WHERE name='waylon'), 7);

INSERT INTO users.profile (id, user_id, client_config) VALUES
  (1, (SELECT id from users.users WHERE name='monty'), '{}'::JSON),
  (2, (SELECT id from users.users WHERE name='quimby'), '{}'::JSON),
  (3, (SELECT id from users.users WHERE name='homer'), '{}'::JSON),
  (4, (SELECT id from users.users WHERE name='lisa'), '{}'::JSON),
  (5, (SELECT id from users.users WHERE name='marge'), '{}'::JSON),
  (6, (SELECT id from users.users WHERE name='bart'), '{}'::JSON),
  (7, (SELECT id from users.users WHERE name='maggie'), '{}'::JSON),
  (8, (SELECT id from users.users WHERE name='abe'), '{}'::JSON),
  (9, (SELECT id from users.users WHERE name='patty'), '{}'::JSON),
  (10, (SELECT id from users.users WHERE name='selma'), '{}'::JSON),
  (11, (SELECT id from users.users WHERE name='ned'), '{}'::JSON),
  (12, (SELECT id from users.users WHERE name='rod'), '{}'::JSON),
  (13, (SELECT id from users.users WHERE name='todd'), '{}'::JSON),
  (14, (SELECT id from users.users WHERE name='maude'), '{}'::JSON),
  (15, (SELECT id from users.users WHERE name='krusty'), '{}'::JSON),
  (16, (SELECT id from users.users WHERE name='apu'), '{}'::JSON),
  (17, (SELECT id from users.users WHERE name='moe'), '{}'::JSON),
  (18, (SELECT id from users.users WHERE name='timothy'), '{}'::JSON),
  (19, (SELECT id from users.users WHERE name='helen'), '{}'::JSON),
  (20, (SELECT id from users.users WHERE name='jessica'), '{}'::JSON),
  (21, (SELECT id from users.users WHERE name='nelson'), '{}'::JSON),
  (22, (SELECT id from users.users WHERE name='milhouse'), '{}'::JSON),
  (23, (SELECT id from users.users WHERE name='edna'), '{}'::JSON),
  (24, (SELECT id from users.users WHERE name='willie'), '{}'::JSON),
  (25, (SELECT id from users.users WHERE name='barney'), '{}'::JSON),
  (26, (SELECT id from users.users WHERE name='waylon'), '{}'::JSON);

SELECT setval('users.profile_id_seq', (SELECT max(id) FROM users.profile), true);


INSERT INTO users.permissions (id, name) VALUES
  (0, 'login'),
  (1, 'edit users'),
  (2, 'list users'),
  (3, 'edit groups'),
  (4, 'list groups'),
  (5, 'edit roles'),
  (6, 'list roles'),
  (7, 'edit alarms'),
  (8, 'list alarms'),
  (9, 'edit layers'),
  (10, 'list layers'),
  (11, 'statistic'),
  (12, 'strokestream'),
  (13, 'alarmstream'),
  (14, 'userlog'),
  (15, 'localshapes');

SELECT setval('users.permissions_id_seq', (SELECT max(id) FROM users.permissions), true);

INSERT INTO users.permission_activity (permission_id, activity_id) VALUES
  (0, 0),
  (0, 22),
  (0, 65),
  (0, 67),
  (1, 2),
  (1, 3),
  (1, 4),
  (1, 24),
  (1, 32),
  (1, 33),
  (1, 34),
  (2, 1),
  (2, 14),
  (2, 25),
  (2, 30),
  (2, 32),
  (3, 6),
  (3, 7),
  (3, 8),
  (3, 23),
  (3, 28),
  (3, 29),
  (3, 50),
  (3, 51),
  (3, 53),
  (4, 5),
  (4, 15),
  (4, 25),
  (4, 26),
  (4, 27),
  (4, 49),
  (4, 52),
  (5, 10),
  (5, 11),
  (5, 12),
  (5, 19),
  (5, 21),
  (6, 9),
  (6, 13),
  (6, 16),
  (6, 17),
  (6, 18),
  (6, 20),
  (7, 31),
  (7, 33),
  (7, 35),
  (7, 36),
  (7, 37),
  (7, 40),
  (7, 41),
  (7, 42),
  (8, 30),
  (8, 32),
  (8, 34),
  (8, 38),
  (8, 39),
  (8, 43),
  (8, 59),
  (8, 60),
  (8, 61),
  (8, 62),
  (8, 63),
  (9, 45),
  (9, 46),
  (9, 48),
  (9, 68),
  (10, 44),
  (10, 47),
  (11, 54),
  (12, 55),
  (12, 56),
  (13, 57),
  (13, 58),
  (14, 64);

INSERT INTO users.role_permissions (role_id, permission_id) VALUES
  (1, 0),
  (1, 1),
  (1, 2),
  (1, 3),
  (1, 4),
  (1, 5),
  (1, 6),
  (2, 0),
  (2, 1),
  (2, 2),
  (2, 3),
  (2, 4),
  (2, 5),
  (2, 6),
  (3, 0),
  (3, 1),
  (3, 2),
  (3, 3),
  (3, 4),
  (3, 5),
  (3, 6),
  (3, 7),
  (3, 8),
  (3, 9),
  (3, 10),
  (3, 11),
  (3, 12),
  (3, 13),
  (3, 14),
  (3, 15),
  (4, 0),
  (4, 2),
  (5, 0),
  (5, 2),
  (7, 0);


-- area definitions

INSERT INTO alarm.alarm_areas (id, geom, alarm_duration, radius, center, icon, area, inner_radius, stroke_threshold) VALUES
-- Wuppertal
(1, ST_GeomFromText('MULTIPOLYGON(((7.13567595834915 51.2536732175099,7.15417615507982 51.2578448304982,7.16106838523438 51.2498643534771,7.16233800657864 51.2420652509338,7.15508302746857 51.2344475228682,7.14528880566998 51.2300945354021,7.13440633700489 51.2315455312242,7.12787685580583 51.2435162467558,7.13567595834915 51.2536732175099)))', 4326), 1, 0, NULL, DEFAULT, DEFAULT, DEFAULT, DEFAULT),
-- Leverkusen
(2, ST_GeomFromText('MULTIPOLYGON(((7.10792566325314 51.1552775633296,7.19861290212897 51.1567285591516,7.22491220140296 50.9677363533344,6.98440964390427 50.9699128470674,6.9441445098434 51.1647090361727,7.10792566325314 51.1552775633296)))', 4326), 5, 0, NULL, DEFAULT, DEFAULT, DEFAULT, DEFAULT),
-- Ruhrgebiet, dreiteilig
(3, ST_GeomFromText('MULTIPOLYGON(((6.87737036226864 51.4667995647729,6.8772853429822 51.4682108849279,6.88061809901088 51.468806019933,6.88526015205084 51.4710845368098,6.88638240663193 51.471390606241,6.88645042206108 51.4705744210911,6.8804310565807 51.46844893893,6.87861164385076 51.4674117036353,6.87737036226864 51.4667995647729)),((6.89937335360089 51.4731590073991,6.8947823121328 51.4727509148241,6.89292889168827 51.471509633242,6.89648269786172 51.4717476872441,6.90000249632059 51.4723088145346,6.90903154454116 51.473686126975,6.91088496498569 51.4760156554237,6.91399667086962 51.4777500488672,6.90896352911201 51.477682033438,6.90415143749916 51.4755395474196,6.9001385271789 51.4733970614011,6.89937335360089 51.4731590073991)),((6.93268391003047 51.4605591491478,6.92671555612195 51.4561381462526,6.93329604889288 51.4522442629333,6.93810814050573 51.4546758145257,6.94028463423875 51.4586547171314,6.93268391003047 51.4605591491478)))', 4326), 10, 0, NULL, DEFAULT, DEFAULT,DEFAULT, DEFAULT),
-- Ruhrgebiet, Alarmdauer 180
(4, ST_GeomFromText('MULTIPOLYGON(((7.14263053598043 51.5990045551966,7.12630683298278 51.567989519501,7.17310144824271 51.5532981868031,7.20846947140428 51.5854014693652,7.18507216377432 51.6060781598289,7.14263053598043 51.5990045551966)))', 4326), 180, 0, NULL, DEFAULT, DEFAULT,DEFAULT, DEFAULT),
-- Ring um Dortmund
(5, ST_GeomFromText('MULTIPOLYGON(((7.39537587072736 51.5503055079202,7.53956858053992 51.5630924086017,7.59098824498251 51.4961652263114,7.57656897400126 51.4401205126861,7.4492440906196 51.4257012417049,7.37524330369692 51.468686992932,7.37061825451426 51.514665423042,7.39537587072736 51.5503055079202),(7.51045797686078 51.5473128290373,7.41033926514187 51.5372465455221,7.38939051296155 51.5119448058758,7.39428762386085 51.4863710045128,7.42094967209034 51.4605251414332,7.44706759688658 51.4455617470187,7.48515623721442 51.440936697836,7.52814198844157 51.4572604008336,7.54854661718863 51.4697752397985,7.51970807522611 51.4969814114613,7.51780364320972 51.5094962504261,7.56242176473663 51.5277243854402,7.51045797686078 51.5473128290373)))', 4326), 1, 0, NULL, DEFAULT, DEFAULT, DEFAULT, DEFAULT),
-- Gebiet mit Stückelung und Lücken (Bonn)
(6, ST_GeomFromText('MULTIPOLYGON(((6.33759991647971 50.6342737080355,6.26686387015657 50.1848277521669,6.68910365436242 50.1587098273707,7.20928565655416 50.2272693799608,7.19405020042302 50.517831293319,6.89913529959883 50.7202452104898,6.67930943256383 50.7746575538153,6.33759991647971 50.6342737080355),(6.37895329740709 50.6244794862369,6.41921843146796 50.628832473703,6.42574791266702 50.6005380551737,6.39527700040474 50.5863908459091,6.37568855680756 50.5961850677077,6.37895329740709 50.6244794862369),(6.3800415442736 50.4830073935906,6.37460030994105 50.4470952469958,6.39636524727125 50.4503599875953,6.3800415442736 50.4830073935906),(6.69345664182846 50.522184280785,6.54055795708381 50.4193449518998,6.5829995848777 50.3703738429069,6.69345664182846 50.522184280785),(7.05638697180952 50.5042282074876,6.93178270559413 50.5036840840543,6.90620890423115 50.434580408031,7.00197462848402 50.3850651756048,7.14072610396403 50.4046536192019,7.05638697180952 50.5042282074876),(6.54708743828287 50.2593726625229,6.66897108733198 50.2811375998531,6.67767706226406 50.2310782439936,6.60585276907441 50.2114898003964,6.54708743828287 50.2593726625229)),((6.20102493473272 50.1396655072068,6.21408389713084 50.3420794243776,6.122671160344 50.3529618930427,6.04649387968831 50.1919013567992,6.20102493473272 50.1396655072068)),((7.90304303395423 50.9645566320213,7.70715859798245 50.8426729829722,7.80292432223532 50.7469072587193,7.90304303395423 50.9645566320213)))', 4326), 5, 0, NULL, DEFAULT, DEFAULT,DEFAULT, DEFAULT),
-- Braunschweig
(7, ST_GeomFromText('MULTIPOLYGON(((10.4745703795172 52.3205122276926,10.4767468732502 52.1888343568449,10.5833950661682 52.1899226037115,10.5921010411002 52.3183357339596,10.4745703795172 52.3205122276926)))', 4326), 10, 0, NULL, DEFAULT, DEFAULT,DEFAULT, DEFAULT),
--Burgdorf(Hannover)
(8, ST_GeomFromText('MULTIPOLYGON(((9.989756400487 52.4668814312382,9.99247701765328 52.4304251612101,10.0262126705151 52.4277045440439,10.0321980282809 52.4652490609385,9.989756400487 52.4668814312382)))', 4326), 10, 0, NULL, DEFAULT, DEFAULT,DEFAULT, DEFAULT),
-- Lehrte(Hannover)
(9, ST_GeomFromText('MULTIPOLYGON(((9.98540341302096 52.3945130146153,9.95275600702566 52.391792397449,9.94731477269311 52.3787334350509,9.95166776015915 52.3651303492196,9.94459415552684 52.3586008680205,9.96472672255727 52.3558802508542,9.98322691928794 52.3602332383203,9.99683000511932 52.3722039538519,10.0011829925854 52.3868952865498,9.98540341302096 52.3945130146153)))', 4326), 10, 0, NULL, DEFAULT, DEFAULT,DEFAULT, DEFAULT),
-- Hannover
(10, ST_GeomFromText('MULTIPOLYGON(((9.63934090947081 52.4217191862781,9.64859100783614 52.424711865161,9.63961297118743 52.4399473212921,9.67253243889936 52.4372267041258,9.67634130293214 52.4551827774232,9.70436365974478 52.4606240117558,9.74925384298831 52.4628005054888,9.79849701369788 52.4394031978588,9.84202688835828 52.4081161004467,9.88419645443554 52.3915203357324,9.88854944190158 52.3681230281024,9.83604153059247 52.3449977821891,9.80883535892973 52.3338432518074,9.84991667814048 52.2671881312337,9.85345348045663 52.2399819595709,9.79659258168149 52.2696366866833,9.76911434830211 52.3134386230603,9.74462879380564 52.3343873752406,9.71116520266046 52.3235049065755,9.67226037718273 52.310718005894,9.64478214380336 52.3109900676107,9.6521278101523 52.3294902643413,9.66382646396728 52.3539758188378,9.6521278101523 52.3615935469034,9.64151740320383 52.3754686944514,9.63145111968861 52.3966895083483,9.63934090947081 52.4217191862781)))', 4326), 10, 0, NULL, DEFAULT, DEFAULT,DEFAULT, DEFAULT),
-- Seesen (Harz)
(11, ST_GeomFromText('MULTIPOLYGON(((10.1788392935431 51.8999048137866,10.156258171063 51.8985445052034,10.1510889984471 51.8865737896718,10.1464639492644 51.8680735929411,10.1557140476298 51.8574631859927,10.1655082694284 51.8797722467561,10.1902658856415 51.8795001850395,10.2041410331895 51.8835811107889,10.2041410331895 51.8939194560208,10.1902658856415 51.8958238880371,10.1788392935431 51.8999048137866)))', 4326), 10, 0, NULL, DEFAULT, DEFAULT,DEFAULT, DEFAULT),
-- Clausthal- Zellerfeld (Harz)
(12, ST_GeomFromText('MULTIPOLYGON(((10.3624809522666 51.8120288793159,10.3499661133018 51.8136612496156,10.3431645703861 51.8212789776812,10.3216716947725 51.8171980519318,10.3219437564892 51.809036200433,10.3148701518568 51.8003302255009,10.3208555096226 51.7916242505688,10.334458595454 51.7910801271356,10.350510236735 51.7962492997515,10.3627530139833 51.7976096083346,10.3749957912315 51.8022346575173,10.3624809522666 51.8120288793159)))', 4326), 10, 0, NULL, DEFAULT, DEFAULT,DEFAULT, DEFAULT),
-- Goslar (Harz)
(13, ST_GeomFromText('MULTIPOLYGON(((10.4201580361917 51.8996327520699,10.4511730718872 51.9004489372198,10.4511730718872 51.9116034676015,10.4685850217514 51.9159564550676,10.4696732686179 51.9322801580652,10.4500848250207 51.9341845900816,10.4285919494071 51.943434688447,10.411179999543 51.9401699478474,10.4013857777444 51.933912528365,10.4092755675266 51.9137799613346,10.4201580361917 51.8996327520699)))', 4326), DEFAULT, 0, NULL, DEFAULT, DEFAULT, DEFAULT, DEFAULT),
-- Osterwieck
(14, ST_GeomFromText('MULTIPOLYGON(((10.7093596409667 51.9801630201917,10.7014698511845 51.9779865264586,10.6965727402852 51.9692805515266,10.6963006785686 51.957309835995,10.7085434558168 51.9535009719622,10.7232347885147 51.961662823461,10.7297642697137 51.9695526132432,10.7235068502313 51.9763541561589,10.7093596409667 51.9801630201917)))', 4326), DEFAULT, 0, NULL, DEFAULT, DEFAULT, DEFAULT, DEFAULT),
--  Blankenburg (Harz)
(15, ST_GeomFromText('MULTIPOLYGON(((10.9373473595005 51.8136612496156,10.9602005436972 51.8035949661004,10.9879508387932 51.8054993981168,10.9963847520087 51.7935286825852,10.9721712592288 51.7861830162363,10.9656417780298 51.7761167327211,10.9490460133155 51.7804697201871,10.9302737548682 51.7869992013862,10.9251045822523 51.7927124974353,10.9308178783015 51.8011464106508,10.9373473595005 51.8136612496156)))', 4326), DEFAULT, 0, NULL, DEFAULT, DEFAULT, DEFAULT, DEFAULT),
-- Eisenach
(16, ST_GeomFromText('MULTIPOLYGON(((10.3237121576472 50.989586309951,10.3007229425922 50.9973400688749,10.2910647516519 50.9868656927847,10.2653549194306 50.993939297417,10.2422296735173 51.0022371797742,10.2401892106426 50.9958437294334,10.2525680187492 50.9837369830435,10.283719085303 50.977887656136,10.2974582019927 50.9729905452367,10.3090208249494 50.9698618354955,10.3118774729739 50.9619720457133,10.3283372068299 50.9641485394463,10.3398998297866 50.9670051874709,10.3616647671168 50.9589793668304,10.3824774884388 50.9572109656723,10.3858782598966 50.9625161691466,10.3720031123486 50.9682294651957,10.3506462675934 50.9803362115857,10.3405799840781 50.9853693533433,10.3292894228381 50.9923069271173,10.3155503061484 50.9976121305915,10.3109252569657 50.9980202231664,10.3237121576472 50.989586309951)))', 4326), DEFAULT, 0, NULL, DEFAULT, DEFAULT, DEFAULT, DEFAULT),
-- linienhaftes Polygon bei Hildburghausen
(17, ST_GeomFromText('MULTIPOLYGON(((10.6821534693039 50.4443746298295,10.6875266882073 50.4402937040801,10.693444030544 50.4349885006059,10.705142684359 50.4313156674314,10.710379872404 50.4271667262528,10.7169773690323 50.4243780936574,10.7193579090528 50.422473661641,10.7239829582354 50.4212493839162,10.7319407634468 50.4185287667499,10.7326889331675 50.4190728901832,10.7236428810896 50.4230858005034,10.716433245599 50.4270306953945,10.7075912398086 50.4311796365731,10.6942602156939 50.436348809189,10.6887509659322 50.442402182384,10.6831056853121 50.4448507378336,10.6821534693039 50.4443746298295)))', 4326), DEFAULT, 0, NULL, DEFAULT, DEFAULT, DEFAULT, DEFAULT),
--Northeim (Harz)
(18, ST_GeomFromText('MULTIPOLYGON(((9.97180032718959 51.7317026574816,9.98894021533713 51.7258533305741,10.0130176772587 51.7100737510097,10.0122014921088 51.691981646854,9.99927856056897 51.6835477336385,9.97941805525516 51.6785145918809,9.97180032718959 51.6800109313224,9.98390707357952 51.6997354057779,9.97887393182191 51.7156510162006,9.96935177173995 51.7238128676994,9.97180032718959 51.7317026574816)))', 4326), DEFAULT, 0, NULL, DEFAULT, DEFAULT, DEFAULT, DEFAULT),
-- Augsburg
(19, ST_GeomFromText('MULTIPOLYGON(((10.9007062969512 48.195267526464,11.0126814015558 48.3605641094516,10.9023059413027 48.4661406366502,10.8756452021111 48.6111750578523,10.8244565828633 48.6485000927205,10.812725857619 48.403754506942,10.8532501811902 48.3728280494798,10.7897976219143 48.2805818918769,10.7332768548282 48.213930043898,10.9007062969512 48.195267526464)))', 4326), DEFAULT, 0, NULL, DEFAULT, DEFAULT, DEFAULT, DEFAULT),
-- Augsburg Naturpark
(20, ST_GeomFromText('MULTIPOLYGON(((10.6362317641709 48.1040877984288,10.7332768548282 48.213930043898,10.7897976219143 48.2805818918769,10.8532501811902 48.3728280494798,10.812725857619 48.403754506942,10.8244565828633 48.6485000927205,10.7226125591516 48.6149075613391,10.6820882355804 48.61224148742,10.5258563039178 48.5114638932759,10.4090822662588 48.5119971080597,10.3744233053098 48.4816038653813,10.4421415828563 48.4010884330228,10.4186801323678 48.3339033702601,10.4704019663994 48.2693843814165,10.4991955647263 48.223527910007,10.4677358924802 48.1750053646784,10.5205241560795 48.1243499602144,10.5781113527333 48.0891577844815,10.648495704199 48.0608974009385,10.6362317641709 48.1040877984288)))', 4326), 10, 0, NULL, DEFAULT, DEFAULT, DEFAULT, DEFAULT),
-- Berlin
(21, ST_GeomFromText('MULTIPOLYGON(((13.0127700557068 52.7158624637852,13.2612481449721 52.7979775404952,13.644096359763 52.6710724219434,13.7347428730143 52.4172621848397,13.7123478520934 52.2914234958556,13.5182576707788 52.2018434121719,13.3422967921145 52.2231720035252,13.1897973639387 52.2978220732615,13.0916858437138 52.3479442629417,13.0159693444097 52.3799371499716,13.0127700557068 52.5569644582035,13.0127700557068 52.7158624637852)))', 4326), 60, 0, NULL, DEFAULT, DEFAULT, DEFAULT, DEFAULT),
-- Tiergarten in Berlin
(22, ST_GeomFromText('MULTIPOLYGON(((13.3362981257964 52.511107986794,13.3427633550503 52.5087751721148,13.3456960363614 52.5103081646183,13.3496284953921 52.5092417350506,13.3616924798763 52.5097082979865,13.3770890567595 52.5116412015779,13.3772223604554 52.5180397789838,13.3748895457762 52.5205058973591,13.3750895013201 52.5221055417106,13.3704905238095 52.5236385342141,13.3669579758667 52.5222388454065,13.3638919908596 52.5221055417106,13.3541608210547 52.5187729493116,13.3480288510407 52.5194394677914,13.3421634884185 52.5193728159434,13.3388975478675 52.5166400901763,13.3370312961241 52.514373927345,13.3362981257964 52.511107986794)))', 4326), 180, 0, NULL, DEFAULT, DEFAULT, DEFAULT, DEFAULT),
-- Hamburg (teils außerhalb Data_area 4)
(23, ST_GeomFromText('MULTIPOLYGON(((9.64331918482915 53.7796926093765,9.87686726014724 53.6741160821779,9.90779371760945 53.7615633067263,9.9461851820453 53.8852691365751,10.0698910118941 53.8276819399213,10.2042611374196 53.6847803778546,10.2917083619679 53.5493438227614,10.2970405098063 53.4554980208071,10.183998975634 53.3403236274996,10.1242789198449 53.2912678673871,9.98670950561648 53.2731385647368,9.86726939403828 53.2678064168985,9.79048646516658 53.313662888308,9.84594080268503 53.4139072676683,9.8608708166323 53.4522987321041,9.78622074689593 53.4682951756191,9.66358134661474 53.4576308799425,9.71796925456553 53.5024209217843,9.81928006349347 53.5642738367087,9.76489215554268 53.6069310194152,9.6902420858063 53.661318927366,9.65718276920877 53.6997103918018,9.61239272736694 53.7391682858054,9.64331918482915 53.7796926093765)))', 4326), 10, 0, NULL, DEFAULT, DEFAULT, DEFAULT, DEFAULT);

SELECT setval('alarm.alarm_areas_id_seq', (SELECT max(id) FROM alarm.alarm_areas), true);

INSERT INTO alarm.alarm_area_unit (id, name, group_id, is_extended) VALUES
  (1, 'Alle Alarme Ruhrgebiet', (SELECT id from users.groups where name='Alarme West'), TRUE),
  (2, 'Alle Alarme im Ruhrgebiet', (SELECT id from users.groups where name='Alarm-Admin'), TRUE),
  (3, 'Wuppertal', (SELECT id from users.groups where name='Alarme West'), FALSE),
  (4, 'Dortmund Umkreis', (SELECT id from users.groups where name='Alarme West'), FALSE),
  (5, 'Bonn', (SELECT id from users.groups where name='Alarme West'), FALSE),
  (6, 'Umgebung Hannover', (SELECT id from users.groups where name='Alarme Ost'), TRUE),
  (7, 'Umgebung Harz', (SELECT id from users.groups where name='Alarme Ost'), TRUE),
  (8, 'Augsburg', (SELECT id from users.groups where name='Alarme Ost'), FALSE),
  (9, 'Berlin', (SELECT id from users.groups where name='Alarme Ost'), FALSE),
  (10, 'Berlin und Hamburg', (SELECT id from users.groups where name='Alarme Ost'), TRUE),
  (11, 'Berlin-Tiergarten', (SELECT id from users.groups where name='Alarme Ost'), FALSE),
  (12, 'Augsburg mit Naturpark', (SELECT id from users.groups where name='Alarme Ost'), FALSE),
  (13, 'Naturpark bei Augsburg', (SELECT id from users.groups where name='Alarme Ost'), FALSE);

SELECT setval('alarm.alarm_area_unit_id_seq', (SELECT max(id) FROM alarm.alarm_area_unit), true);

INSERT INTO alarm.alarm_area_map (alarm_area_unit_id, alarm_area_id) VALUES
  (1,1), (1,2), (1,3), (1, 4), (1, 5),
  (2,1), (2,2), (2,3), (2, 4), (2, 5),
  (3, 1),
  (4,5),
  (5,6),
  (6,7), (6,8), (6,9), (6,10),
  (7, 11), (7,12), (7,13), (7,14), (7,15), (7, 16), (7, 17), (7, 18),
  (8, 19),
  (9, 21),
  (10, 21), (10,23),
  (11, 22),
  (12, 19), (12, 20),
  (13, 20)
;

INSERT INTO alarm.alarms (user_id, alarm_area_unit_id) values
  ((SELECT id FROM users.users where name = 'homer'), (SELECT id FROM alarm.alarm_area_unit WHERE name = 'Alle Alarme Ruhrgebiet')),
  ((SELECT id FROM users.users where name = 'homer'), (SELECT id FROM alarm.alarm_area_unit WHERE name = 'Wuppertal')),
  ((SELECT id FROM users.users where name = 'homer'), (SELECT id FROM alarm.alarm_area_unit WHERE name = 'Dortmund Umkreis')),
  ((SELECT id FROM users.users where name = 'lisa'), (SELECT id FROM alarm.alarm_area_unit WHERE name = 'Alle Alarme Ruhrgebiet')),
  ((SELECT id FROM users.users where name = 'lisa'), (SELECT id FROM alarm.alarm_area_unit WHERE name = 'Wuppertal')),
  ((SELECT id FROM users.users where name = 'lisa'), (SELECT id FROM alarm.alarm_area_unit WHERE name = 'Dortmund Umkreis')),
  ((SELECT id FROM users.users where name = 'marge') , (SELECT id FROM alarm.alarm_area_unit WHERE name = 'Bonn')),
  ((SELECT id FROM users.users where name = 'apu'), (SELECT id FROM alarm.alarm_area_unit WHERE name = 'Bonn')),
  ((SELECT id FROM users.users where name = 'bart'), (SELECT id FROM alarm.alarm_area_unit WHERE name = 'Alle Alarme im Ruhrgebiet')),
  ((SELECT id FROM users.users where name = 'abe'), (SELECT id FROM alarm.alarm_area_unit WHERE name = 'Dortmund Umkreis')),
  ((SELECT id FROM users.users where name = 'abe'), (SELECT id FROM alarm.alarm_area_unit WHERE name = 'Bonn')),
  ((SELECT id FROM users.users where name = 'patty'), (SELECT id FROM alarm.alarm_area_unit WHERE name = 'Wuppertal')),
  ((SELECT id FROM users.users where name = 'selma'), (SELECT id FROM alarm.alarm_area_unit WHERE name = 'Alle Alarme Ruhrgebiet')),
  ((SELECT id FROM users.users where name = 'rod'), (SELECT id FROM alarm.alarm_area_unit WHERE name = 'Berlin')),
  ((SELECT id FROM users.users where name = 'rod'), (SELECT id FROM alarm.alarm_area_unit WHERE name = 'Berlin und Hamburg')),
  ((SELECT id FROM users.users where name = 'rod'), (SELECT id FROM alarm.alarm_area_unit WHERE name = 'Augsburg')),
  ((SELECT id FROM users.users where name = 'rod'), (SELECT id FROM alarm.alarm_area_unit WHERE name = 'Umgebung Hannover')),
  ((SELECT id FROM users.users where name = 'rod'), (SELECT id FROM alarm.alarm_area_unit WHERE name = 'Umgebung Harz')),
  ((SELECT id FROM users.users where name = 'ned'), (SELECT id FROM alarm.alarm_area_unit WHERE name = 'Berlin-Tiergarten')),
  ((SELECT id FROM users.users where name = 'willie'), (SELECT id FROM alarm.alarm_area_unit WHERE name = 'Naturpark bei Augsburg'));


INSERT INTO users.map_layers (id, geoserver_name, name, baselayer, timelayer, strokelayer, tick_width, layer_type, default_name, external_server, external_parameter) VALUES
  (1, 'google_sat', 'google_sat', true, false, false, null, 'google', 'Google Satellite', NULL, NULL),
  (2, 'IR_WMST_euro', 'IR_WMST_euro', false, true, false, 1800000, 'wmst', 'IR_WMST_euro', 'https://geoserver.blids.net/geoserver/wms?', '{"layers": "IR_WMST_euro","transparent": true,"format": "image/png"}'::JSON),
  (3, 'blitze:blitzewmst', 'blitze:blitzewmst', false, true, true, null, 'wmst', 'wms-T Blitze', 'https://geoserver.blids.net/geoserver/wms?', '{"layers": "blitze:blitzewmst","transparent": true,"format": "image/png"}'::JSON);

SELECT setval('users.map_layers_id_seq', (SELECT max(id) FROM users.map_layers), true);

INSERT INTO users.group_map_layer (
  group_id, map_layer_id, display_name, checked_on_login) VALUES
  ((SELECT id FROM users.groups WHERE name='Superadmin'), 1, 'Google Satellite', TRUE),
  ((SELECT id FROM users.groups WHERE name='Superadmin'), 2, 'Cloud Cover', TRUE),
  ((SELECT id FROM users.groups WHERE name='Superadmin'), 3, 'Blitze', TRUE),
  ((SELECT id FROM users.groups WHERE name='Alarm-Admin'), 1, 'Google', TRUE),
  ((SELECT id FROM users.groups WHERE name='Alarm-Admin'), 2, 'Wolken', FALSE),
  ((SELECT id FROM users.groups WHERE name='Alarm-Admin'), 3, 'Blitze', TRUE),
  ((SELECT id FROM users.groups WHERE name='Blitzadmin'), 1, 'Google Satellite', TRUE),
  ((SELECT id FROM users.groups WHERE name='Blitzadmin'), 3, 'Blitze', FALSE),
  ((SELECT id FROM users.groups WHERE name='Blitzadmin'), 2, 'Regenradar', FALSE),
  ((SELECT id FROM users.groups WHERE name='Alarme West'), 1, 'Google Satellite', TRUE),
  ((SELECT id FROM users.groups WHERE name='Alarme West'), 3, 'Blitze', TRUE),
  ((SELECT id FROM users.groups WHERE name='Blitze West'), 1, 'Google Satellite', TRUE),
  ((SELECT id FROM users.groups WHERE name='Blitze West'), 3, 'Live-Blitze', TRUE),
  ((SELECT id FROM users.groups WHERE name='Blitze West'), 2, 'Wolken', TRUE),
  ((SELECT id FROM users.groups WHERE name='Blitze Ost'), 1, 'Google Satellite', TRUE),
  ((SELECT id FROM users.groups WHERE name='Blitze Ost'), 2, 'Wolken', FALSE),
  ((SELECT id FROM users.groups WHERE name='Blitze Ost'), 3, 'Live-Blitze', TRUE),
  ((SELECT id FROM users.groups WHERE name='Alarme Ost'), 1, 'Google Satellite', TRUE),
  ((SELECT id FROM users.groups WHERE name='Alarme Ost'), 2, 'Wolken', TRUE),
  ((SELECT id FROM users.groups WHERE name='Alarme Ost'), 3, 'Wolken', TRUE),
  ((SELECT id FROM users.groups WHERE name='Alarm-Admin Paderborn'), 1, 'Google', TRUE),
  ((SELECT id FROM users.groups WHERE name='Blitzadmin Paderborn'), 1, 'Google', TRUE),
  ((SELECT id FROM users.groups WHERE name='Blitzanzeige (Westen)'), 1, 'Google Satellite', TRUE),
  ((SELECT id FROM users.groups WHERE name='Blitzanzeige (Westen)'), 2, 'Wolken', TRUE),
  ((SELECT id FROM users.groups WHERE name='Blitzanzeige (Westen)'), 3, 'Blitze', TRUE),
  ((SELECT id FROM users.groups WHERE name='Blitzanzeige (Osten)'), 1, 'Google Satellite', TRUE),
  ((SELECT id FROM users.groups WHERE name='Blitzanzeige (Osten)'), 2, 'Wolken', TRUE),
  ((SELECT id FROM users.groups WHERE name='Blitzanzeige (Osten)'), 3, 'Blitze', TRUE),
  ((SELECT id FROM users.groups WHERE name='Blitzanzeige (Gesamt)'), 1, 'Google Satellite', TRUE),
  ((SELECT id FROM users.groups WHERE name='Blitzanzeige (Gesamt)'), 3, 'Blitze', TRUE),
  ((SELECT id FROM users.groups WHERE name='Blitzanzeige (Paderborn)'), 1, 'Google Satellite', TRUE),
  ((SELECT id FROM users.groups WHERE name='Blitzanzeige (Paderborn)'), 3, 'Blitze', TRUE);

SELECT setval('users.group_map_layer_id_seq', (SELECT max(id) FROM users.group_map_layer), true);

INSERT INTO users.host_themes (id, host, data) VALUES
(1, 'ball-lightning-backend:5000', '{
  "title":"Blids Live",
  "logo":"Siemens-logo.svg",
  "links":[{
    "name":"cookie",
    "url":"https://www.siemens.de/cookie-policy"
  },{
    "name":"imprint",
    "url": "https://www.siemens.de/impressum"
  },{
    "name": "dataProtection",
    "url":"https://www.siemens.de/Datenschutz"
  },{
    "name": "termsOfUse",
    "url":"https://www.siemens.de/nutzungsbedingungen"
  }],
  "theme":{
    "dark": false,
    "themes": {
      "light": {
        "primary": {
        "base": "#afafaf",
        "darken1": "#008888",
        "lighten1": "#a0dcdc"
      },
      "secondary": "#8585F4",
      "accent": "#82B1FF",
      "error": "#444444",
      "info": "#33b5e5",
      "success": "#00C851",
      "warning": "#ffbb33"
    },
    "dark": {
      "primary": {
        "base": "#009999",
        "darken1": "#008888",
        "lighten1": "#a0dcdc"
      },
      "secondary": "#8585F4",
      "accent": "#82B1FF",
      "error": "#444444",
      "info": "#33b5e5",
      "success": "#00C851",
      "warning": "#ffbb33"
    }
  }
}}'::JSON);

INSERT INTO users.alarm_templates_mail (id, alarm_subject, allclear_subject, alarm_text, allclear_text) VALUES
  (1,'Ball-Lightning: Alarm', 'Ball-Lightning: Entwarnung', '%TYP (%DATUM %ZEIT) : %BREITE°N, %LÄNGE°O schlug in %GEBIET ein. Alarm für %GRUPPE, Blitzdichte %DICHTE, %ANZAHL Blitze', 'Ball-Lightning: Entwarnung nach %MINUTEN Minuten für %GEBIET (%LÄNGE°O, %BREITE°N)'),
  (2, 'Blitz-Alarm','Blitz-Entwarnung', '%GEBIET: Gewitter mit Blitzen seit %ZEIT', 'Der Alarm für %GEBIET lief aus, es wurden keine weiteren Blitze registriert.');

insert into users.alarm_templates_sms (id, alarm_text, allclear_text) values
(1, '%TYP (%DATUM %ZEIT) : %BREITE°N, %LÄNGE°O', '%TYP (%DATUM %ZEIT) : %BREITE°N, %LÄNGE°O');

insert into users.alarm_templates_fax (id, alarm_subject, allclear_subject, alarm_text, allclear_text) values
(1, 'Ball-Lightning: Alarm','Ball-Lightning: Entwarnung', '%TYP (%DATUM %ZEIT) : %BREITE°N, %LÄNGE°O schlug in %GEBIET ein. Alarm für %GRUPPE, Blitzdichte %DICHTE, %ANZAHL Blitze', 'Ball-Lightning: Entwarnung nach %MINUTEN Minuten für %GEBIET (%LÄNGE°O, %BREITE°N)'),
(2, 'Blitz-Alarm','Blitz-Entwarnung', '%GEBIET: Gewitter mit Blitzen seit %ZEIT', 'Der Alarm für %GEBIET lief aus, es wurden keine weiteren Blitze registriert.');

--TODO INSERT INTO alarm_template_mqtt_id (id, alarm_subject, allclear_subject, alarm_text, allclear_text)

INSERT INTO users.alarm_settings (
  id, alarm_template_mail_id, email, alarm_template_sms_id, sms, alarm_template_fax_id, fax, phone, start_date, stop_date, start_time, stop_time, mon, tue, wed, thu, fri, sat, sun, only_log, stroke_protocol, stroke_as_csv) VALUES
  ((SELECT id FROM users.users WHERE name = 'homer'), 2, 'homer@example.org',1, '555-67890', null, '', '', TIMESTAMP '2020-01-01 00:00:00', (now() + '1 month'::interval), DEFAULT, DEFAULT, TRUE, TRUE, TRUE, TRUE, TRUE, TRUE, TRUE, FALSE, FALSE, FALSE),
  ((SELECT id FROM users.users WHERE name = 'lisa'), 1, 'lisa@example.org', null, '', null, '', '555-444-333',TIMESTAMP '2020-01-01 00:00:00',(now() + '1 month'::interval), '07:00:00'::time,'22:00:00'::time, TRUE, TRUE, TRUE, TRUE, TRUE, TRUE, TRUE, FALSE, FALSE, FALSE),
  ((SELECT id FROM users.users WHERE name = 'marge'), null, '', 1, '555-098765', null, '', '',TIMESTAMP '2020-01-01 00:00:00',(now() + '1 month'::interval), '06:00:00'::time,'22:30:00'::time, TRUE, TRUE, TRUE, TRUE, TRUE, TRUE, TRUE, FALSE, FALSE, FALSE),
  ((SELECT id FROM users.users WHERE name = 'apu'), null, 'apu@example.org', null, '', 1, '567567567-0', '',TIMESTAMP '2020-01-01 00:00:00',(now() + '1 month'::interval), DEFAULT, DEFAULT, TRUE, TRUE, TRUE, TRUE, TRUE, FALSE, FALSE, FALSE, FALSE, FALSE),
  ((SELECT id FROM users.users WHERE name = 'bart'), 2, 'bart@example.org', 1, '0123-456789', 1, '0123457777', '',TIMESTAMP '2020-01-01 00:00:00',(now() + '1 month'::interval), DEFAULT, DEFAULT, TRUE, TRUE, TRUE, TRUE, TRUE, FALSE, FALSE, FALSE, FALSE, FALSE),
  ((SELECT id FROM users.users WHERE name = 'abe'), 2, 'abe@example.org',1, '222-333444555', 1, '222-600', '',TIMESTAMP '2020-01-01 00:00:00',(now() + '1 month'::interval), DEFAULT, DEFAULT, TRUE, TRUE, TRUE, TRUE, TRUE, TRUE, TRUE, FALSE, FALSE, FALSE),
  ((SELECT id FROM users.users WHERE name = 'patty'),1 , 'patty@example.org',1, '35353535', null, '','',TIMESTAMP '2020-01-01 00:00:00',(now() + '1 month'::interval), DEFAULT, DEFAULT, TRUE, TRUE, TRUE, TRUE, TRUE, TRUE, TRUE, FALSE, FALSE, FALSE),
  ((SELECT id FROM users.users WHERE name = 'rod'), 1, 'rod@rxample.org',1, '77777777', 1, '','',TIMESTAMP '2020-01-01 00:00:00',(now() + '1 month'::interval), '11:00:00'::time,'18:30:00'::time, TRUE, TRUE, TRUE, TRUE, TRUE, TRUE, TRUE, FALSE, FALSE, FALSE),
  ((SELECT id FROM users.users WHERE name = 'ned'), 1, 'ned@example.org',1, '123456789', null, '','987654321',TIMESTAMP '2020-01-01 00:00:00',(now() + '1 month'::interval), '03:00:00'::time,'22:45:00'::time, TRUE, FALSE, TRUE, FALSE, TRUE, TRUE, TRUE, FALSE, FALSE, FALSE),
  ((SELECT id FROM users.users WHERE name = 'willie'), null, '',1, '555-555555', null, '', '',TIMESTAMP '2020-01-01 00:00:00',(now() + '1 month'::interval), DEFAULT, DEFAULT, FALSE, TRUE, TRUE, TRUE, TRUE, TRUE, TRUE, FALSE, FALSE, FALSE);

-- helper function to direct a stroke to the general bounding box given by the
-- geometry. Does not always hit the geometry itself.
-- use e.g. alarm.aimforarea((SELECT geom from alarm.alarm_areas where id = 1),
-- now());
CREATE OR REPLACE FUNCTION alarm.aimforarea(geom public.geometry,ts timestamp with time zone)
 RETURNS integer
 LANGUAGE plpgsql
AS $function$
BEGIN
  insert into blitz.blitze (eintrag, koordinate, typ, strom, zeit) values (
  (SELECT now()),
  (SELECT st_setSRID(
    st_makePoint(
          (
            (st_xmax(st_extent(geom)) - st_xmin(st_extent(geom))) * random()
          ) + st_xmin(st_extent(geom)),
          (
            (st_ymax(st_extent(geom)) - st_ymin(st_extent(geom))) * random()
          ) + st_ymin(st_extent(geom))
         ), 4326)
        ),
        (SELECT round(random()) * 3 + 1),
        (SELECT -1000 +( 2000*random())),
        ts
      );
   RETURN (select count(*) from blitz.blitze);
END
$function$;

-- insert 5000 strokes in the last three days at different areas
CREATE OR REPLACE FUNCTION alarm.fire_strokes()
  RETURNS VOID
   LANGUAGE plpgsql
AS $function$
BEGIN
  FOR a in 1..5000 LOOP
    PERFORM alarm.aimforarea(
      (SELECT geom from blitz.data_areas where id = 2),
      (SELECT now() - interval '3 days' + (259200 * random() * interval '1 second'))
    );
  END LOOP;
END
$function$;

SELECT alarm.fire_strokes();
