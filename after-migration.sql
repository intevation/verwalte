INSERT INTO users.permissions (id, name) VALUES
  (0, 'login'),
  (1, 'edit users'),
  (2, 'list users'),
  (3, 'edit groups'),
  (4, 'list groups'),
  (5, 'edit roles'),
  (6, 'list roles'),
  (7, 'edit alarms'),
  (8, 'list alarms'),
  (9, 'edit layers'),
  (10, 'list layers'),
  (11, 'statistic'),
  (12, 'strokestream'),
  (13, 'alarmstream'),
  (14, 'userlog'),
  (15, 'localshapes');

SELECT setval('users.permissions_id_seq', (SELECT max(id) FROM users.permissions), true);

INSERT INTO users.permission_activity (permission_id, activity_id) VALUES
  (0, 0),
  (0, 22),
  (0, 65),
  (0, 67),
  (1, 2),
  (1, 3),
  (1, 4),
  (1, 24),
  (1, 32),
  (1, 33),
  (1, 34),
  (2, 1),
  (2, 14),
  (2, 25),
  (2, 30),
  (2, 32),
  (3, 6),
  (3, 7),
  (3, 8),
  (3, 23),
  (3, 28),
  (3, 29),
  (3, 50),
  (3, 51),
  (3, 53),
  (4, 5),
  (4, 15),
  (4, 25),
  (4, 26),
  (4, 27),
  (4, 49),
  (4, 52),
  (5, 10),
  (5, 11),
  (5, 12),
  (5, 19),
  (5, 21),
  (6, 9),
  (6, 13),
  (6, 16),
  (6, 17),
  (6, 18),
  (6, 20),
  (7, 31),
  (7, 33),
  (7, 35),
  (7, 36),
  (7, 37),
  (7, 40),
  (7, 41),
  (7, 42),
  (8, 30),
  (8, 32),
  (8, 34),
  (8, 38),
  (8, 39),
  (8, 43),
  (8, 59),
  (8, 60),
  (8, 61),
  (8, 62),
  (8, 63),
  (9, 45),
  (9, 46),
  (9, 48),
  (10, 44),
  (10, 47),
  (11, 54),
  (12, 55),
  (12, 56),
  (13, 57),
  (13, 58),
  (14, 64);

-- TODO Mapping
INSERT INTO users.host_themes (id, host, data) VALUES
(1, 'ball-lightning-backend:5000', '{
  "title":"Blids Live",
  "logo":"Siemens-logo.svg",
  "links":[{
    "name":"cookie",
    "url":"https://www.siemens.de/cookie-policy"
  },{
    "name":"imprint",
    "url": "https://www.siemens.de/impressum"
  },{
    "name": "dataProtection",
    "url":"https://www.siemens.de/Datenschutz"
  },{
    "name": "termsOfUse",
    "url":"https://www.siemens.de/nutzungsbedingungen"
  }],
  "theme":{
    "dark": false,
    "themes": {
      "light": {
        "primary": {
        "base": "#afafaf",
        "darken1": "#008888",
        "lighten1": "#a0dcdc"
      },
      "secondary": "#8585F4",
      "accent": "#82B1FF",
      "error": "#444444",
      "info": "#33b5e5",
      "success": "#00C851",
      "warning": "#ffbb33"
    },
    "dark": {
      "primary": {
        "base": "#009999",
        "darken1": "#008888",
        "lighten1": "#a0dcdc"
      },
      "secondary": "#8585F4",
      "accent": "#82B1FF",
      "error": "#444444",
      "info": "#33b5e5",
      "success": "#00C851",
      "warning": "#ffbb33"
    }
  }
}}'::JSON);


--TODO: Mapping
INSERT INTO users.themes (id, name, data) VALUES
  (1, 'generic', '{
    "theme":{
      "dark": false,
      "themes": {
        "light": {
          "primary": {
            "base": "#afafaf",
            "darken1": "#008888",
            "lighten1": "#a0dcdc"
          },
          "secondary": "#8585F4",
          "accent": "#82B1FF",
          "error": "#444444",
          "info": "#33b5e5",
          "success": "#00C851",
          "warning": "#ffbb33"
        },
        "dark": {
          "primary": {
            "base": "#009999",
            "darken1": "#008888",
            "lighten1": "#a0dcdc"
          },
          "secondary": "#8585F4",
          "accent": "#82B1FF",
          "error": "#444444",
          "info": "#33b5e5",
          "success": "#00C851",
          "warning": "#ffbb33"
        }
      }
    }
  }'),
  (2, 'Admin-Theme', '{
  "theme":{
    "dark": false,
    "themes": {
      "light": {
        "primary": {
          "base": "#afafaf",
          "darken1": "#008888",
          "lighten1": "#a0dcdc"
        },
        "secondary": "#8585F4",
        "accent": "#82B1FF",
        "error": "#444444",
        "info": "#33b5e5",
        "success": "#00C851",
        "warning": "#ffbb33"
      },
      "dark": {
        "primary": {
          "base": "#009999",
          "darken1": "#008888",
          "lighten1": "#a0dcdc"
        },
        "secondary": "#8585F4",
        "accent": "#82B1FF",
        "error": "#444444",
        "info": "#33b5e5",
        "success": "#00C851",
        "warning": "#ffbb33"
      }
    }
  }
  }'),
  (3, 'Theme For Western Users', '{
  "theme":{
    "dark": true,
    "themes": {
      "light": {
        "primary": {
          "base": "#afafff",
          "darken1": "#0088f8",
          "lighten1": "#a0dcfc"
        },
        "secondary": "#8f85F4",
        "accent": "#8fB1FF",
        "error": "#4f4444",
        "info": "#3fb5e5",
        "success": "#0fC851",
        "warning": "#f0bb33"
      },
      "dark": {
        "primary": {
          "base": "#009999",
          "darken1": "#008888",
          "lighten1": "#a0dcdc"
        },
        "secondary": "#8585F4",
        "accent": "#82B1FF",
        "error": "#444444",
        "info": "#33b5e5",
        "success": "#00C851",
        "warning": "#ffbb33"
      }
    }
  }
  }'),
  (4, 'Theme For Eastern Users', '{
  "theme":{
    "dark": false,
    "themes": {
      "light": {
        "primary": {
          "base": "#ffafaf",
          "darken1": "#a08888",
          "lighten1": "#aadcdc"
        },
        "secondary": "#8585F4",
        "accent": "#82B1FF",
        "error": "#444444",
        "info": "#33b5e5",
        "success": "#00C851",
        "warning": "#ffbb33"
      },
      "dark": {
        "primary": {
          "base": "#009999",
          "darken1": "#008888",
          "lighten1": "#a0dcdc"
        },
        "secondary": "#8585F4",
        "accent": "#82B1FF",
        "error": "#444444",
        "info": "#33b5e5",
        "success": "#00C851",
        "warning": "#ffbb33"
      }
    }
  }
  }');

UPDATE users.groups SET theme_id = 1;


-- Role "Admin All Rights" (id=4) is root
UPDATE users.roles SET parent = 4 WHERE id = 1;
UPDATE users.roles SET parent = 4 WHERE id = 3;
UPDATE users.roles SET parent = 4 WHERE id = 6;
UPDATE users.roles SET parent = 4 WHERE id = 7;
UPDATE users.roles SET parent = 4 WHERE id = 10;
UPDATE users.roles SET parent = 4 WHERE id = 11;
UPDATE users.roles SET parent = 4 WHERE id = 12;
UPDATE users.roles SET parent = 4 WHERE id = 14;
UPDATE users.roles SET parent = 4 WHERE id = 16;
UPDATE users.roles SET parent = 4 WHERE id = 19;
UPDATE users.roles SET parent = 4 WHERE id = 22;
UPDATE users.roles SET parent = 4 WHERE id = 24;
UPDATE users.roles SET parent = 4 WHERE id = 25;
UPDATE users.roles SET parent = 1 WHERE id = 2;
UPDATE users.roles SET parent = 2 WHERE id = 13;
UPDATE users.roles SET parent = 4 WHERE id = 12;
UPDATE users.roles SET parent = 1 WHERE id = 12;
UPDATE users.roles SET parent = 7 WHERE id = 18;
UPDATE users.roles SET parent = 18 WHERE id = 17;
UPDATE users.roles SET parent = 1 WHERE id = 20;
UPDATE users.roles SET parent = 1 WHERE id = 23;
UPDATE users.roles SET parent = 1 WHERE id = 27;
UPDATE users.roles SET parent = 27 WHERE id = 5;
UPDATE users.roles SET parent = 1 WHERE id = 15;
UPDATE users.roles SET parent = 4 WHERE id = 26;


INSERT INTO users.role_permissions (role_id, permission_id) VALUES
  (1, 0),
  (1, 2),
  (1, 4),
  (1, 6),
  (1, 7),
  (1, 8),
  (1, 10),
  (1, 11),
  (1, 12),
  (1, 13),
  (1, 14),
  (2, 0),
  (2, 1),
  (2, 2),
  (2, 3),
  (2, 4),
  (2, 6),
  (2, 7),
  (2, 8),
  (2, 9),
  (2, 10),
  (2, 11),
  (2, 12),
  (2, 13),
  (2, 14),
  (3, 0),
  (3, 1),
  (3, 2),
  (3, 4),
  (3, 6),
  (3, 8),
  (3, 10),
  (3, 11),
  (3, 12),
  (3, 13),
  (3, 14),
  (4, 0),
  (4, 1),
  (4, 2),
  (4, 3),
  (4, 4),
  (4, 5),
  (4, 6),
  (4, 7),
  (4, 8),
  (4, 9),
  (4, 10),
  (4, 11),
  (4, 12),
  (4, 13),
  (4, 14),
  (6, 0),
  (6, 1),
  (6, 2),
  (6, 4),
  (6, 6),
  (6, 8),
  (6, 10),
  (6, 11),
  (6, 12),
  (6, 13),
  (6, 14),
  (7, 0),
  (7, 1),
  (7, 2),
  (7, 3),
  (7, 4),
  (7, 6),
  (7, 7),
  (7, 8),
  (7, 10),
  (7, 11),
  (7, 12),
  (7, 13),
  (7, 14),
  (10, 0),
  (10, 1),
  (10, 2),
  (10, 3),
  (10, 4),
  (10, 5),
  (10, 6),
  (10, 8),
  (10, 10),
  (10, 11),
  (10, 12),
  (10, 13),
  (10, 14),
  (11, 0),
  (11, 1),
  (11, 2),
  (11, 3),
  (11, 4),
  (11, 6),
  (11, 7),
  (11, 8),
  (11, 9),
  (11, 10),
  (11, 11),
  (11, 12),
  (11, 13),
  (11, 14),
  (12, 0),
  (12, 1),
  (12, 2),
  (12, 3),
  (12, 4),
  (12, 6),
  (12, 7),
  (12, 8),
  (12, 9),
  (12, 10),
  (12, 11),
  (12, 12),
  (12, 13),
  (12, 14),
  (12, 5),
  (13, 0),
  (13, 1),
  (13, 2),
  (13, 4),
  (13, 6),
  (13, 7),
  (13, 8),
  (13, 10),
  (13, 11),
  (13, 12),
  (13, 13),
  (13, 14),
  (14, 0),
  (14, 1),
  (14, 2),
  (14, 3),
  (14, 4),
  (14, 5),
  (14, 6),
  (14, 7),
  (14, 8),
  (14, 9),
  (14, 10),
  (14, 11),
  (14, 12),
  (14, 13),
  (14, 14),
  (15, 0),
  (15, 2),
  (15, 4),
  (15, 6),
  (15, 8),
  (15, 10),
  (15, 11),
  (15, 12),
  (15, 13),
  (15, 14),
  (16, 0),
  (16, 2),
  (16, 4),
  (16, 6),
  (16, 8),
  (16, 10),
  (16, 11),
  (16, 12),
  (16, 13),
  (16, 14),
  (17, 0),
  (17, 1),
  (17, 2),
  (17, 3),
  (17, 4),
  (17, 6),
  (17, 8),
  (17, 10),
  (17, 11),
  (17, 12),
  (17, 13),
  (17, 14),
  (18, 0),
  (18, 1),
  (18, 2),
  (18, 3),
  (18, 4),
  (18, 6),
  (18, 7),
  (18, 8),
  (18, 10),
  (18, 11),
  (18, 12),
  (18, 13),
  (18, 14),
  (19, 0),
  (19, 1),
  (19, 2),
  (19, 3),
  (19, 4),
  (19, 5),
  (19, 6),
  (19, 7),
  (19, 8),
  (19, 9),
  (19, 10),
  (19, 11),
  (19, 12),
  (19, 13),
  (19, 14),
  (20, 0),
  (20, 2),
  (20, 4),
  (20, 6),
  (20, 7),
  (20, 8),
  (20, 10),
  (20, 11),
  (20, 12),
  (20, 13),
  (20, 14),
  (21, 0),
  (21, 1),
  (21, 2),
  (21, 3),
  (21, 4),
  (21, 5),
  (21, 6),
  (21, 7),
  (21, 8),
  (21, 9),
  (21, 10),
  (21, 11),
  (21, 12),
  (21, 13),
  (21, 14),
  (22, 0),
  (22, 1),
  (22, 2),
  (22, 3),
  (22, 4),
  (22, 5),
  (22, 6),
  (22, 7),
  (22, 8),
  (22, 9),
  (22, 10),
  (22, 11),
  (22, 12),
  (22, 13),
  (22, 14),
  (23, 0),
  (23, 2),
  (23, 4),
  (23, 6),
  (23, 7),
  (23, 8),
  (23, 10),
  (23, 11),
  (23, 12),
  (23, 13),
  (23, 14),
  (24, 0),
  (24, 1),
  (24, 2),
  (24, 3),
  (24, 4),
  (24, 6),
  (24, 7),
  (24, 8),
  (24, 9),
  (24, 10),
  (24, 11),
  (24, 12),
  (24, 13),
  (24, 14),
  (25, 0),
  (25, 1),
  (25, 2),
  (25, 3),
  (25, 4),
  (25, 6),
  (25, 7),
  (25, 8),
  (25, 9),
  (25, 10),
  (25, 11),
  (25, 12),
  (25, 13),
  (25, 14),
  (27, 0),
  (27, 2),
  (27, 4),
  (27, 6),
  (27, 7),
  (27, 8),
  (27, 10),
  (27, 11),
  (27, 12),
  (27, 13),
  (27, 14);

INSERT INTO users.profile (user_id, client_config) SELECT u.id, '{}'::JSON FROM users.users u;
