-- sql statements to test the triggers on a demo database. Needs a readily set up blids database.
-- Do NOT use in production
-- ./install-db.sh --demo
-- psql -f tests/schema_triggers.sql blids

insert into blitz.stroke_areas values
    (1, ST_GeomFromText('POLYGON((7.1 51.5, 7.2 51.5, 7.2 51.6, 7.1 51.6, 7.1 51.5))', 4326) ),
    (2, ST_GeomFromText('POLYGON((7.2 51.5,7.3 51.5,7.3 51.6,7.2 51.6,7.2 51.5))', 4326) ),
    (3, ST_GeomFromText('POLYGON((7.1 51.5,7.3 51.5,7.3 51.6,7.1 51.6,7.1 51.5))', 4326) ),
    -- inside eastern area, 1 sq km reprojected
    (4, ST_Transform( ST_GeomFromText('POLYGON((4127000 3164000,4128000 3164000,4128000 3163000,4127000 3163000,4127000 3164000))', 3035), 4326 ) );

INSERT INTO alarm.alarm_areas VALUES
    -- 1: western area
    (1, ST_GeomFromText('MULTIPOLYGON(((7.1 51.5,7.2 51.5,7.2 51.6,7.1 51.6,7.1 51.5)))', 4326), 0, null, null, null, 0, null, null),
    -- 2: eastern area, common border with western area
    (2, ST_GeomFromText('MULTIPOLYGON(((7.2 51.5,7.3 51.5,7.3 51.6,7.2 51.6,7.2 51.5)))', 4326), 0, null, null, null, 0, null, null),
    -- 3: combination of East/west area, with alarm duration 10 minutes
    (3, ST_GeomFromText('MULTIPOLYGON(((7.1 51.5,7.3 51.5,7.3 51.6,7.1 51.6,7.1 51.5)))', 4326), 2, null, null, null, 0, null, null),
    -- 4: 1 square kilometre inside eastern area, originally in projection EPSG:3035
    (4, ST_Transform(
        ST_GeomFromText('MULTIPOLYGON(((4127000 3164000,4128000 3164000,4128000 3163000,4127000 3163000,4127000 3164000)))', 3035),
        4326), 10, null, null, null, 0, null, null),
    -- 5 western area, with hole
    (5, ST_GeomFromText('MULTIPOLYGON(((7.1 51.5,7.2 51.5,7.2 51.6,7.1 51.6,7.1 51.5),(7.13 51.53,7.17 51.53,7.17 51.57,7.13 51.57,7.13 51.53)))', 4326), 2, null, null, null, 0, null, null),
    -- 11 12: smaller area within western area (12 with stroke threshold of 5)
    (11, ST_GeomFromText('MULTIPOLYGON(((7.1 51.5,7.13 51.5,7.13 51.52,7.1 51.52,7.1 51.5)))', 4326), 0, null, null, null, 0, 0, 1),
    (12, ST_GeomFromText('MULTIPOLYGON(((7.1 51.5,7.13 51.5,7.13 51.52,7.1 51.52,7.1 51.5)))', 4326), 0, null, null, null, 0, 0, 5);

INSERT INTO users.groups VALUES
    (DEFAULT, NULL, 'WEST', NULL, 1, NULL, 900000, NULL, NULL, TRUE),
    (DEFAULT, NULL, 'EAST', NULL, 2, NULL, 900000, NULL, NULL, TRUE),
    (DEFAULT, NULL, 'EASTWEST', NULL, 3, NULL, 900000, NULL, NULL, TRUE),
    (DEFAULT, NULL, 'withintarget', NULL, 4, NULL, 900000, NULL, NULL, TRUE);


INSERT INTO users.users VALUES
    (DEFAULT, 'west_mon_wed', MD5('wes3t3'), (SELECT id from users.groups WHERE name='WEST'), DEFAULT, DEFAULT, 'Western User alarm mon-wed', 'lastName', DEFAULT, DEFAULT, DEFAULT, DEFAULT, DEFAULT, DEFAULT, DEFAULT, DEFAULT, DEFAULT),
    (DEFAULT, 'west_thu_sat', MD5('wes3t3'), (SELECT id from users.groups WHERE name='WEST'), DEFAULT, DEFAULT, 'Western User alarm thu-sat', 'lastName', DEFAULT, DEFAULT, DEFAULT, DEFAULT, DEFAULT, DEFAULT, DEFAULT, DEFAULT, DEFAULT),
    (DEFAULT, 'eastwest', MD5('wes1t3'), (SELECT id from users.groups WHERE name='EASTWEST'), DEFAULT, DEFAULT, 'global User', 'lastName', DEFAULT, DEFAULT, DEFAULT, DEFAULT, DEFAULT, DEFAULT, DEFAULT, DEFAULT, DEFAULT),
    (DEFAULT, 'east', MD5('ea4st5'), (SELECT id from users.groups WHERE name='EAST'), DEFAULT, DEFAULT, 'Eastern User', 'lastName', DEFAULT, DEFAULT, DEFAULT, DEFAULT, DEFAULT, DEFAULT, DEFAULT, DEFAULT, DEFAULT),
    (DEFAULT, 'withintarget', MD5('with11n3'), (SELECT id from users.groups WHERE name='withintarget'), DEFAULT, DEFAULT, 'Target User', 'lastName', DEFAULT, DEFAULT, DEFAULT, DEFAULT, DEFAULT, DEFAULT, DEFAULT, DEFAULT, DEFAULT);

insert into users.alarm_templates_mail
(id, alarm_text, allclear_text) values
(1, '%TYP (%DATUM %ZEIT) : %BREITE°N, %LÄNGE°O schlug in %GEBIET ein.Alarm für %GRUPPE, Blitzdichte %DICHTE, %ANZAHL Blitze'
, '%TYP (%DATUM %ZEIT) : Gebiet %GEBIET');

insert into users.alarm_templates_sms (id, alarm_text, allclear_text) values
(1, '%TYP (%DATUM %ZEIT) : %BREITE°N, %LÄNGE°O', '%TYP (%DATUM %ZEIT) : %BREITE°N, %LÄNGE°O');

insert into users.alarm_settings (id, start_date, stop_date, thu, fri, sat, sun, alarm_template_mail_id, email) values
((select id from users.users where name ='west_mon_wed'), now(), (now() + '1 week'::interval), false, false, false, false, 1, 'mail@example.com');

insert into users.alarm_settings (id, start_date, stop_date, sun, mon, tue, wed, alarm_template_sms_id, sms) values
((select id from users.users where name ='west_thu_sat'), now(), (now() + '1 week'::interval), false, false, false, false, 1, '555-5554321');

insert into users.alarm_settings (id, start_date, stop_date) values
((select id from users.users where name ='east'), now(), (now() + '1 week'::interval) );

insert into users.alarm_settings (id, start_date, stop_date, alarm_template_mail_id, email, alarm_template_sms_id, sms) values
((select id from users.users where name ='eastwest'), now(), (now() + '1 week'::interval), 1, 'mail2@example.com', 1, '555878787');

insert into users.alarm_settings (id, start_date, stop_date) values
((select id from users.users where name ='withintarget'), now(), (now() + '1 week'::interval) );

create function testStrokes() returns void
LANGUAGE plpgsql
    AS $$
BEGIN
-- no alarms should be active in the areas 1 .. 5

-- check: entry of an old stroke does not trigger current counters
insert into blitz.strokes (coordinate, entry) VALUES (ST_GeomFromText('POINT(7.111 51.5551)', 4326), TIMESTAMP '2003-10-02 10:22:32');
assert (select stroke_counter from alarm.stroke_counter where id = 2) = 0, 'old stroke counted as new (1)';
assert (select stroke_counter from alarm.stroke_counter where id = 3) = 0, 'old stroke counted as new (2)';

-- adding 2 strokes in the west, one within the hole of area 5
insert into blitz.strokes (coordinate) VALUES (ST_GeomFromText('POINT(7.111 51.5551)', 4326));
insert into blitz.strokes (coordinate) VALUES (ST_GeomFromText('POINT(7.135 51.563)', 4326));
assert (select stroke_counter from alarm.stroke_counter where id = 2) = 0, 'eastern stroke triggered';
assert (select stroke_counter from alarm.stroke_counter where id = 1) = 2, 'western strokes did not trigger';
assert (select stroke_counter from alarm.stroke_counter where id = 3) = 2, 'total area miscount';
assert (select stroke_counter from alarm.stroke_counter where id = 5) = 1, 'hole miscount';
-- adding stroke outside the areas 1 .. 5
insert into blitz.strokes (coordinate) VALUES (ST_GeomFromText('POINT(7.302 51.601)', 4326));
assert (select stroke_counter from alarm.stroke_counter where id = 1) = 2, 'stroke outside area counted as west';
assert (select stroke_counter from alarm.stroke_counter where id = 2) = 0, 'stroke outside area counted as east';
assert (select stroke_counter from alarm.stroke_counter where id = 3) = 2, 'stroke outside area counted as east/west';
assert (select stroke_counter from alarm.stroke_counter where id = 5) = 1, 'stroke outside area counted as within west with hole';
-- adding stroke in the east
insert into blitz.strokes (coordinate) VALUES (ST_GeomFromText('POINT(7.202 51.515)', 4326));
assert (select stroke_counter from alarm.stroke_counter where id = 1) = 2, 'western stroke miscount';
assert (select stroke_counter from alarm.stroke_counter where id = 2) = 1, 'eastern stroke miscount';
assert (select stroke_counter from alarm.stroke_counter where id = 3) = 3, 'total stroke miscount';
assert (select stroke_counter from alarm.stroke_counter where id = 5) = 1, 'stroke in ring triggered alarm';

-- check if an area (see alarm_area 4) correctly reprojects and that a stroke hits the target
assert (select stroke_counter from alarm.stroke_counter where id = 4) = 0, 'target area not empty';
insert into blitz.strokes (coordinate) VALUES (ST_GeomFromText('POINT(7.20967350413072 51.5488315757396)', 4326));
assert (select stroke_counter from alarm.stroke_counter where id = 4) = 1, 'stroke hits target area';

-- TODO corner case: trigger exactly on the border of EAST and WEST (eastern part: not hit ?)
-- insert into blitz.strokes (coordinate) VALUES (ST_GeomFromText('POINT(51.525 7.2000 )', 4326));
END
$$;

-- check if the displayed area of a geometry updates accordingly
create function testAreaCalculation() returns void
LANGUAGE plpgsql
    AS $$
BEGIN
-- area recalculation triggers correctly after insert and update on alarm areas:
-- using alarm_area 4
assert
    (select area from alarm.alarm_areas where id = 4 ) = 1000000.00318301;
update alarm.alarm_areas set geom = ST_Transform(
    ST_GeomFromText(
        'MULTIPOLYGON(((4127500 3164000,4128000 3164000,4128000 3163000,4127500 3163000,4127500 3164000)))', 3035),4326)
        where id = 4;
assert
    (select area from alarm.alarm_areas where id = 4 ) = 499999.997970283;

-- resetting to 'original' value
update alarm.alarm_areas set geom = ST_Transform(
        ST_GeomFromText('MULTIPOLYGON (((4127000 3164000,4128000 3164000,4128000 3163000,4127000 3163000,4127000 3164000)))', 3035),4326)
        where id = 4;
assert
    (select area from alarm.alarm_areas where id = 4 ) = 1000000.00318301;
END
$$;

create function triggerAlarms() returns void
LANGUAGE plpgsql
    AS $$
BEGIN
    perform alarm.expire_alarms();
    assert (select count(*) from alarm.notifications) = 0, 'initial notifications present';
    assert (select alarm from alarm.alarm_area_unit where name = 'Lower Southwest non extended') = false, 'alarm set';
    assert (select alarm from alarm.alarm_area_unit where name = 'Lower Southwest non extended') = false, 'alarm set';
    assert (select alarm from alarm.alarm_area_unit where name = 'West extended') = false, 'alarm set';
    assert (select alarm from alarm.alarm_area_unit where name = 'West extended, long alarm') = false, 'alarm set';
    insert into blitz.strokes (coordinate) VALUES (ST_GeomFromText('POINT(7.112 51.566)', 4326));
    assert (select alarm from alarm.alarm_area_unit where name = 'Lower Southwest non extended') = false, 'alarm set';
    assert (select alarm from alarm.alarm_area_unit where name = 'West extended') = true, 'alarm not triggered';
    assert (select alarm from alarm.alarm_area_unit where name = 'West extended, long alarm') = true, 'alarm not triggered';
    assert (select count(*) from alarm.notifications where type = 'alarm') = 2, 'miscount of triggered alarms';
    insert into blitz.strokes (coordinate) VALUES (ST_GeomFromText('POINT(7.120 51.5114)', 4326));
    assert (select count(*) from alarm.notifications where type = 'alarm') = 3, 'non-extended alarm did not trigger';
    perform alarm.expire_alarms();
    -- TODO: does not expire in time
    -- assert (select alarm from alarm.alarm_area_unit where name = 'Lower Southwest non extended') = false, 'alarm not expired (a)';
    -- assert (select count(*) from alarm.notifications where alarm =
    --     ( SELECT id from alarm.alarms where alarm_area_unit_id = 100)) = 2, 'non-extended alarm did not clear';
    -- assert (select alarm from alarm.alarm_area_unit where name = 'West extended') = false, 'alarm not expired (b)';
    -- assert (select count(*) from alarm.notifications where alarm = 1 ) = 2, 'non-extended alarm did not clear';
    -- assert (select alarm from alarm.alarm_area_unit where name = 'West extended, long alarm') = true, 'alarm expires too early';

    -- testing stroke_threshold. area 12 should not be alerted on the first 4 strokes
    assert (select stroke_counter from alarm.stroke_counter where id = 12) = 0, 'previous strokes';
    assert (select alarm from alarm.alarm_area_unit where name = 'West extended, threshold 5 strokes') = false, 'alarm triggered, no strokes registered';
    insert into blitz.strokes (coordinate) VALUES
        (ST_GeomFromText('POINT(7.118 51.5114)', 4326)),
        (ST_GeomFromText('POINT(7.12521 51.5164)', 4326)),
        (ST_GeomFromText('POINT(7.12857 51.50199)', 4326)),
        (ST_GeomFromText('POINT(7.10143 51.51886)', 4326));
    assert (select stroke_counter from alarm.stroke_counter where id = 12) = 4, 'strokes did not hit proper area';
    assert (select alarm from alarm.alarm_area_unit where name = 'West extended, threshold 5 strokes') = false, 'alarm triggered before threshold met';
    insert into blitz.strokes (coordinate) VALUES (ST_GeomFromText('POINT(7.11977 51.50582)', 4326));
    assert (select stroke_counter from alarm.stroke_counter where id = 12) = 5, 'fifth stroke did not hit proper area';
    assert (select alarm from alarm.alarm_area_unit where name = 'West extended, threshold 5 strokes') = true, 'alarm not triggered even if threshold met';
    perform alarm.expire_alarms();
END
$$;

select teststrokes();
select testAreaCalculation();

insert into alarm.alarm_area_unit values
    (100, 'Lower Southwest non extended', (select id from users.groups where name = 'WEST'), DEFAULT, null, false, DEFAULT),
    (101, 'West extended', (select id from users.groups where name = 'WEST'), DEFAULT, null, TRUE, DEFAULT),
    (102, 'West extended, long alarm', (select id from users.groups where name = 'EASTWEST'), DEFAULT, null, TRUE, DEFAULT),
    (103, 'West extended, threshold 5 strokes', (select id from users.groups where name = 'EASTWEST'), DEFAULT, null, TRUE, DEFAULT);

insert into alarm.alarms values
    (default, 100, (select id from users.users where name = 'eastwest')),
    (default, 101, (select id from users.users where name = 'eastwest')),
    (default, 102, (select id from users.users where name = 'eastwest')),
    (default, 103, (select id from users.users where name = 'eastwest'));

insert into alarm.alarm_area_map values
    (11, 100), (1, 101), (3,102), (12, 103);

select triggerAlarms();
