CREATE EXTENSION postgis;

CREATE SCHEMA blitz;
CREATE SCHEMA users;
CREATE SCHEMA alarm;

-- Type and enum definitions
CREATE TYPE alarm.alarm_type AS ENUM (
    'alarm',
    'clear',
    'prewarning',
    'testalarm',
    'testclearing',
    'testprewarning'
);

CREATE TYPE alarm.message_type AS ENUM (
    'sms',
    'phone',
    'email',
    'fax',
    'mqtt',
    'any'
);

CREATE TYPE alarm.conf_option AS ENUM (
    'Debug',
    'SmtpHost',
    'SmtpPort',
    'SmtpHelo',
    'SmtpUser',
    'SmtpPasswd',
    'MailFrom',
    'EcallUser',
    'EcallPasswd',
    'EcallVoiceFrom',
    'EcallFaxFrom',
    'MQTTHost',
    'MQTTPort',
    'MQTTUsername',
    'MQTTPassword',
    'MQTTUseSSL',
    'MQTTCertPath'
);

CREATE TYPE alarm.send_state AS ENUM (
    'ignored',
    'success',
    'partial success',
    'error'
);

-- Functions

-- on each incoming stroke: update the counter in all affected alarm_areas
CREATE FUNCTION blitz.insert_stroke()
    RETURNS trigger
    LANGUAGE 'plpgsql'
    COST 100
    VOLATILE NOT LEAKPROOF SECURITY DEFINER
AS $BODY$
DECLARE
  _area record;
BEGIN
  FOR _area IN (
      -- TODO_MK using stroke.entry or stroke.timestamp ?
      SELECT aa.id, NEW.zeit from alarm.alarm_areas aa
      WHERE st_within(NEW.koordinate, aa.geom)
      AND (extract(epoch from now() - NEW.zeit) /60) <= aa.alarm_duration
  )

  LOOP
    UPDATE alarm.stroke_counter sc
    SET last_stroke = _area.zeit, stroke_counter = stroke_counter + 1
    WHERE sc.id = _area.id;
  END LOOP;
  RETURN NEW;
END;
$BODY$;


-- on update of strokes,broadcast the stroke to blids channel
CREATE FUNCTION blitz.notify_stroke()
    RETURNS trigger
    LANGUAGE 'plpgsql'
    COST 100
    VOLATILE NOT LEAKPROOF
AS $BODY$
DECLARE
 _data jsonb;
BEGIN
    _data := json_build_object (
    'koordinate', st_astext(NEW.koordinate),
    'typ', NEW.typ,
    'strom', NEW.strom,
	  'eintrag', NEW.eintrag,
	  'tlp', NEW.tlp,
	  'sensorzahl', NEW.sensorzahl,
    'zeit', NEW.zeit)::jsonb;
    PERFORM pg_notify('blids',
      json_build_object ('type', 'stroke',
                         'data', _data)::text);
  RETURN NEW;
END;
$BODY$;

-- if an area is updated (new stroke incoming), send alerts to all users
CREATE FUNCTION alarm.alarm() RETURNS trigger
    LANGUAGE plpgsql
    AS $$
DECLARE
  _alarmrec RECORD;
  _units RECORD;
  _aa RECORD;
BEGIN
    IF ( NEW.stroke_counter >= OLD.stroke_counter ) THEN
        FOR _aa IN (
            SELECT aa.id as aa_id, au.id as au_id, au.is_extended, au.alarm, au.notified, aa.stroke_threshold as threshold
            FROM alarm.alarm_areas aa
            JOIN alarm.alarm_area_map am ON NEW.id = am.alarm_area_id
            JOIN alarm.alarm_area_unit au ON am.alarm_area_unit_id = au.id
            WHERE aa.id = NEW.id
        )
        LOOP
            IF _aa.threshold IS NULL OR NEW.stroke_counter >= _aa.threshold THEN
              -- TODO_MK these are two conditions doing the same thing. Also check if every alarm is hit that needs to be hit
              -- It is a combined area, that has not been notified jet.
              IF ( _aa.alarm IS false AND _aa.is_extended IS true AND _aa.notified IS false) THEN
                  NEW.alarm = true;
                  FOR _alarmrec in (SELECT id FROM alarm.alarms WHERE alarm_area_unit_id = _aa.au_id)
                  LOOP
                      EXECUTE 'INSERT INTO alarm.notifications (id, alarm, type, area) values (DEFAULT,' || _alarmrec.id ||', ''alarm'', '|| _aa.aa_id || ')';
                  END LOOP;
                  EXECUTE 'UPDATE alarm.alarm_area_unit SET notified = true, alarm = true, alarm_timestamp = NOW() where id = ' || _aa.au_id;
              -- It is a single area, that has to be notified.
              ELSIF ( _aa.alarm IS false AND _aa.is_extended IS false ) THEN
                  NEW.alarm = true;
                  FOR _alarmrec in (SELECT id FROM alarm.alarms WHERE alarm_area_unit_id = _aa.au_id)
                  LOOP
                      EXECUTE 'INSERT INTO alarm.notifications (id, alarm, type, area) values (DEFAULT,' || _alarmrec.id || ',''alarm'', '||_aa.aa_id||')';
                  END LOOP;
                  EXECUTE 'UPDATE alarm.alarm_area_unit SET notified = true, alarm = true, alarm_timestamp = NOW() where id = ' || _aa.au_id;
              END IF;
            END IF;
        END LOOP;
    END IF;
    RETURN NEW;
END;
$$;


-- remove alerts for areas that have no strokes for more than configured time
CREATE OR REPLACE FUNCTION alarm.expire_alarms()
 RETURNS VOID
 LANGUAGE plpgsql
AS $function$
DECLARE
  _d RECORD;
  _u RECORD;
  _alarmid integer;
BEGIN
    -- Reset stroke counter an alarm status for all areas where no new
    -- strokes were registered for the max. alert time (`alarmdauer'):
    FOR _d IN
        (SELECT aa.alarm_duration, sc.stroke_counter, sc.alarm, sc.id as sc_id, au.id as au_id,
        au.is_extended
         FROM alarm.alarm_areas aa
         JOIN alarm.stroke_counter sc ON aa.id = sc.id
         JOIN alarm.alarm_area_map am ON am.alarm_area_id = aa.id
         JOIN alarm.alarm_area_unit au ON au.id = am.alarm_area_unit_id
          WHERE sc.stroke_counter > 0
          AND (
            EXTRACT(EPOCH FROM now() - last_stroke)::int/60 > aa.alarm_duration
            OR aa.alarm_duration = 0
          )
        )
    LOOP
      UPDATE alarm.stroke_counter SET stroke_counter = 0, alarm = FALSE
        WHERE id = _d.sc_id;
      IF _d.is_extended is false THEN
          SELECT id FROM alarm.alarms INTO _alarmid WHERE alarm_area_unit_id = _d.au_id;
          EXECUTE 'INSERT INTO alarm.notifications (id, alarm, type) values (DEFAULT,' || _alarmid ||', ''clear'')';
      END IF;
    END LOOP;

    -- Reset all combined units where all areas are clear.
    FOR _u IN
        (SELECT * FROM alarm.alarm_area_unit
         WHERE is_extended = TRUE AND alarm = TRUE
        )
    LOOP
        IF (
            (SELECT count(*) FROM alarm.alarm_areas aa
                JOIN alarm.stroke_counter sc ON aa.id = sc.id
                JOIN alarm.alarm_area_map am ON aa.id = am.alarm_area_id
                JOIN alarm.alarm_area_unit au ON au.id = am.alarm_area_unit_id
                WHERE au.id = _u.id AND sc.alarm IS true) = 0
            ) THEN
            UPDATE alarm.alarm_area_unit
              SET alarm = false, notified = false
              WHERE id = _u.id;

        END IF;
    END LOOP;
END;
$function$;


-- returns the day of week of a date. internal use
CREATE FUNCTION alarm.dayofweek(_time TIMESTAMP WITH TIME ZONE) RETURNS character varying
    LANGUAGE plpgsql
    AS $$
DECLARE
 dayofweek varchar(3);
 result int;
BEGIN
  SELECT extract (isodow from _time) INTO result;
 IF (result = 1) THEN
         dayofweek := 'mon';
        ELSIF (result = 2) THEN
         dayofweek := 'tue';
        ELSIF (result = 3) THEN
         dayofweek := 'wed';
        ELSIF (result = 4) THEN
         dayofweek := 'thu';
        ELSIF (result = 5) THEN
         dayofweek := 'fri';
        ELSIF (result = 6) THEN
         dayofweek := 'sat';
        ELSE
         dayofweek := 'sun';
        END IF;
 RETURN dayofweek;
END;
$$;

-- build and send a notification to a user
CREATE FUNCTION alarm.notify_alarm() RETURNS trigger
    LANGUAGE plpgsql
    AS $$
DECLARE
  _matching_dow bool;
  _msg_data     jsonb;
  _alarm_r      record;
  _recipient_r  record;
  _r_settings   record;
BEGIN
  SELECT * INTO _alarm_r FROM alarm.alarms
    WHERE id = NEW.alarm;
  SELECT * INTO _recipient_r FROM users.users
    WHERE id = _alarm_r.user_id;
  SELECT * INTO _r_settings FROM users.alarm_settings
    WHERE id = _alarm_r.user_id;
  IF (SELECT COUNT(*) FROM users.alarm_settings WHERE id = _alarm_r.user_id) = 0 THEN
    NEW.done := now();
    NEW.status := 'ignored';
    NEW.status_info := 'No alarm settings for recipient.';
  ELSE
    -- check for matching week day and alarm times:
    EXECUTE 'SELECT '
        || alarm.dayofweek(now())
        || ' FROM users.alarm_settings'
        || ' WHERE id = ' || _alarm_r.user_id
        INTO _matching_dow;
    -- Check if alarm message is still open.
    -- This is so that other triggers could process  before us.
    IF NEW.done IS NULL THEN
      IF NEW.date NOT BETWEEN _recipient_r.start_date AND (_recipient_r.stop_date + interval '1' day)
    -- check subscription period
          -- check day time period
        OR NEW.date NOT BETWEEN
          (date_trunc('day', NEW.date) + _r_settings.start_time) AND
          (date_trunc('day', NEW.date) + _r_settings.stop_time)
        -- check day of week
        OR NOT _matching_dow
        --   TODO_MK old: filter out those without any alarm messages set ?
       -- IF (NEW.type IN ('alarm', 'testalarm')
        --        AND _recipient_r.alarmierung <> '' IS NOT TRUE)
        --    OR (NEW.type IN ('clear', 'testclearing')
        --        AND _recipient_r.entwarnung <> '' IS NOT TRUE)
      THEN
        NEW.done := now();
        NEW.status := 'ignored';
        NEW.status_info := 'Notification not desired by recipient.';
      ELSE
        _msg_data := json_build_object (
            'id', NEW.id,
            'groupids',
          (SELECT array_to_json(array_agg(parent))
                                             FROM users.groups ug
                                             WHERE _recipient_r.group_id = ug.id)
        );
        _msg_data := json_build_object ('type', NEW.type,
                                        'data', _msg_data);
        PERFORM pg_notify('blids', _msg_data::text);
      END IF;
    END IF;
  END IF;
  RETURN NEW;
END;
$$;

CREATE FUNCTION alarm.get_alarm_msg(_id integer)
 RETURNS text
 LANGUAGE plpgsql
 SECURITY DEFINER
AS $function$
DECLARE
  _msg_data     jsonb;
  _template_t text;
  _template_s text;
  _message_r    record;
  _recipient_r  record;
  _r_settings   record;
  _alarm_r      record;
  _area_wkt     text;
BEGIN
  SELECT * INTO _message_r FROM alarm.notifications
    WHERE id = _id;
  SELECT * INTO _alarm_r FROM alarm.alarms
    WHERE id = _message_r.alarm;
  SELECT * INTO _recipient_r FROM users.users
    WHERE id = _alarm_r.user_id;
  SELECT * into _r_settings FROM users.alarm_settings
    WHERE id = _alarm_r.user_id;
  _msg_data := json_build_object ('recipient', _recipient_r.id,
                                  'logonly', _r_settings.only_log);
  -- the following code is ugly, hints on how to make it less redundant
  -- are highly appreciated... <wilde@intevation.de>

  IF _r_settings.email <> '' AND _r_settings.alarm_template_mail_id IS NOT NULL
  THEN
      IF _message_r.type IN ('alarm', 'testalarm') THEN
        SELECT alarm_subject from users.alarm_templates_mail INTO _template_s
        WHERE id = _r_settings.alarm_template_mail_id;
        SELECT alarm_text from users.alarm_templates_mail INTO _template_t
        WHERE id = _r_settings.alarm_template_mail_id;
      ELSIF _message_r.type IN ('clear', 'testclearing') THEN
        SELECT allclear_subject from users.alarm_templates_mail INTO _template_s
        WHERE id = _r_settings.alarm_template_mail_id;
        SELECT allclear_text from users.alarm_templates_mail INTO _template_t
        WHERE id = _r_settings.alarm_template_mail_id;
      END IF;
    _msg_data := _msg_data
                || json_build_object ('email', _r_settings.email)::jsonb
                || json_build_object ('email-template', alarm.fill_template(
                                      _alarm_r.alarm_area_unit_id,
                                      _recipient_r.group_id,
                                      _message_r.date,
                                      _message_r.type,
                                      (_r_settings.time_zone = 'UTC'),
                                      _template_t,
                                      _template_s
                                    ))::jsonb;
  END IF;

  IF _r_settings.sms <> ''  AND _r_settings.alarm_template_sms_id IS NOT NULL
  THEN
        IF _message_r.type IN ('alarm', 'testalarm') THEN
        SELECT alarm_subject from users.alarm_templates_sms INTO _template_s
        WHERE id = _r_settings.alarm_template_sms_id;
        SELECT alarm_text from users.alarm_templates_sms INTO _template_t
        WHERE id = _r_settings.alarm_template_sms_id;
      ELSIF _message_r.type IN ('clear', 'testclearing') THEN
        SELECT allclear_subject from users.alarm_templates_sms INTO _template_s
        WHERE id = _r_settings.alarm_template_sms_id;
        SELECT allclear_text from users.alarm_templates_sms INTO _template_t
        WHERE id = _r_settings.alarm_template_sms_id;
      END IF;
    _msg_data := _msg_data
                || json_build_object ('sms', _r_settings.sms)::jsonb
                || json_build_object ('sms-template', alarm.fill_template(
                                      _alarm_r.alarm_area_unit_id,
                                      _recipient_r.group_id,
                                      _message_r.date,
                                      _message_r.type,
                                      (_r_settings.time_zone = 'UTC'),
                                      _template_t,
                                      _template_s
                  ))::jsonb;
  END IF;

  IF _r_settings.fax <> '' AND _r_settings.alarm_template_fax_id IS NOT NULL
  THEN
            IF _message_r.type IN ('alarm', 'testalarm') THEN
        SELECT alarm_subject from users.alarm_templates_fax INTO _template_s
        WHERE id = _r_settings.alarm_template_fax_id;
        SELECT alarm_text from users.alarm_templates_fax INTO _template_t
        WHERE id = _r_settings.alarm_template_fax_id;
      ELSIF _message_r.type IN ('clear', 'testclearing') THEN
        SELECT allclear_subject from users.alarm_templates_fax INTO _template_s
        WHERE id = _r_settings.alarm_template_fax_id;
        SELECT allclear_text from users.alarm_templates_fax INTO _template_t
        WHERE id = _r_settings.alarm_template_fax_id;
      END IF;
    _msg_data := _msg_data
            || json_build_object ('fax', _r_settings.fax)::jsonb
            || json_build_object ('fax-template', alarm.fill_template(
                                  _alarm_r.alarm_area_unit_id,
                                  _recipient_r.group_id,
                                      _message_r.date,
                                      _message_r.type,
                                      (_r_settings.time_zone = 'UTC'), -- TODO_MK: not used
                                      _template_t,
                                      _template_s
                                  ))::jsonb;
  END IF;
  IF _r_settings.mqtt <> '' AND _r_settings.alarm_template_mqtt_id IS NOT NULL
  THEN
      IF _message_r.type IN ('alarm', 'testalarm') THEN
        SELECT alarm_subject from users.alarm_templates_mqtt INTO _template_s
        WHERE id = _r_settings.alarm_template_mqtt_id;
        SELECT alarm_text from users.alarm_templates_mqtt INTO _template_t
        WHERE id = _r_settings.alarm_template_mqtt_id;
      ELSIF _message_r.type IN ('clear', 'testclearing') THEN
        SELECT allclear_subject from users.alarm_templates_mqtt INTO _template_s
        WHERE id = _r_settings.alarm_template_mqtt_id;
        SELECT allclear_text from users.alarm_templates_mqtt INTO _template_t
        WHERE id = _r_settings.alarm_template_mqtt_id;
      END IF;
  _msg_data := _msg_data
          || json_build_object ('mqtt', _r_settings.fax)::jsonb
          || json_build_object ('mqtt-template', alarm.fill_template(
          _alarm_r.alarm_area_unit_id,
          _recipient_r.group_id,
              _message_r.date,
              _message_r.type,
              (_r_settings.time_zone = 'UTC'),
              _template_t,
              _template_s
          ))::jsonb;
  END IF;
  IF _r_settings.phone <> ''
    THEN
      _msg_data := _msg_data
                || json_build_object ('phone', _r_settings.phone)::jsonb;
  END IF;
  RETURN _msg_data::text;
END;
$function$;

-- TODO_MK meaning/handling of UTC (unused at the moment)?
-- TODO_MK check if still valid: "sent before last stroke, so manually count +1"
CREATE FUNCTION alarm.fill_template(
        _au_id integer,
        _group_id integer,
        date timestamp WITH TIME ZONE,
        type alarm.alarm_type,
        UTC bool,
        _templ text,
        _subject text
    ) RETURNS jsonb
LANGUAGE plpgsql
AS $function$

DECLARE
 __Deci_Fmt CONSTANT text := '990.000';
 __Date_Fmt CONSTANT text := 'DD.MM.YYYY';
 __Time_Fmt CONSTANT text := 'HH24:MI:SS';
 _area_r    record;
 _gruppe    text;
 _anzahl    text;
 _dichte    text;
 _jdata     jsonb;

BEGIN
  SELECT au.name, st_x(st_centroid(aa.geom)) AS centerx,
         st_y(st_centroid(aa.geom)) AS centery,
         aa.radius, aa.area,
         sc.stroke_counter,
         aa.stroke_threshold,
         aa.alarm_duration
  INTO _area_r
  FROM alarm.alarm_area_map am
    JOIN alarm.alarm_areas aa ON aa.id = am.alarm_area_id
    JOIN alarm.stroke_counter sc ON aa.id = sc.id
    JOIN alarm.alarm_area_unit au ON am.alarm_area_unit_id = au.id
  WHERE am.alarm_area_unit_id = _au_id;
  SELECT name FROM users.groups INTO _gruppe WHERE id = _group_id;
  -- As the alarm is triggered _before_ the latest stroke is inserted
  -- we have to add one to "blitzanzahl".
  _anzahl := (_area_r.stroke_counter + 1)::text;
  _dichte := ((_area_r.stroke_counter + 1) * 10000 / _area_r.area)::text;

  _templ := replace(_templ, '%TYP', type::text);
  _templ := replace(_templ, '%DATUM', to_char(date, __Date_Fmt));
  _templ := replace(_templ, '%ZEIT', to_char(date, __Time_Fmt));
  _templ := replace(_templ, '%LÄNGE', COALESCE(_area_r.centerx::text, ''));
  _templ := replace(_templ, '%BREITE', COALESCE(_area_r.centery::text, ''));
  _templ := replace(_templ, '%GEBIET', COALESCE(_area_r.name, ''));
  _templ := replace(_templ, '%GRUPPE', COALESCE(_gruppe, ''));
  _templ := replace(_templ, '%RADIUS', COALESCE(_area_r.radius::text, ''));
  _templ := replace(_templ, '%SCHWELLE', COALESCE(_area_r.stroke_threshold::text, ''));
  _templ := replace(_templ, '%ANZAHL', COALESCE(_anzahl::text, ''));
  _templ := replace(_templ, '%DICHTE', COALESCE(_dichte::text, ''));
  _templ := replace(_templ, '%MINUTEN', COALESCE(_area_r.alarm_duration::text, ''));

  _jdata := json_build_object ('type', type::text,
                               'time', date,
                               'lon', _area_r.centerx::float,
                               'lat', _area_r.centery::float,
                               'alarmarea', COALESCE(_area_r.name, ''),
                               'group', COALESCE(_gruppe, ''),
                               'radius', _area_r.radius::float,
                               'threshold', _area_r.stroke_threshold::int,
                               'count', _anzahl::int,
                               'density', _dichte::float,
                               'alarm_duration', _area_r.alarm_duration::int);

  RETURN json_build_object (
                            'subject', _subject,
                            'body', _templ,
                            'raw_data', _jdata);
END;
$function$;

-- (re)calculate the area of an alarm layer.
CREATE FUNCTION alarm.update_surface_area() RETURNS trigger
    LANGUAGE plpgsql SECURITY DEFINER
    AS $$
BEGIN
    NEW.area = st_area(geography(NEW.geom));
    RETURN NEW;
END;
$$;

CREATE OR REPLACE FUNCTION alarm.get_alarm_geom(_id integer)
 RETURNS text
 LANGUAGE plpgsql
 SECURITY DEFINER
AS $function$
DECLARE
  _alarm_r   record;
  _gebiet_id int;
  _gebiet_r  record;
BEGIN

  SELECT aa.id as id, aa.geom as geom, sc.stroke_counter INTO _alarm_r
    FROM alarm.notifications n
         join alarm.alarms a on a.id = n.alarm join alarm.alarm_areas aa on n.alarm_area = aa.id join alarm.stroke_counter sc on aa.id = sc.id
    WHERE n.id = _id AND a.id = n.alarm;

  SELECT stroke_count AS count,
         st_astext(geom) AS geom
    INTO _gebiet_r
    FROM _alarm_r;

  RETURN row_to_json(_gebiet_r);
END;
$function$;

CREATE TYPE public.liveblitz AS
(
	id integer,
	zeit timestamp with time zone,
	typ integer,
	strom numeric,
	koordinate geometry,
	tlp integer,
	unixzeit integer
);

CREATE OR REPLACE FUNCTION blitz.blitz_userid(
_userid integer,
_zeit1 integer,
_zeit2 integer,
_co_left numeric,
_co_right numeric,
_co_top numeric,
_co_bottom numeric)
    RETURNS SETOF liveblitz
    LANGUAGE 'plpgsql'
    COST 100
    STABLE PARALLEL UNSAFE
    ROWS 1000
AS $BODY$
DECLARE
  _zeiten         record;
  _areaofinterest geometry;
BEGIN
  SELECT now() - make_interval(secs => _zeit1) AS startzeit,
         now() - make_interval(secs => _zeit2) AS stopzeit
    INTO _zeiten;

  SELECT st_intersection(
    st_transform(st_makeenvelope(_co_left, _co_top, _co_right, _co_bottom, 3857), 4326),
    s.geom)
  FROM users.users b JOIN users.groups g ON b.group_id = g.id JOIN blitz.stroke_areas s on g.id = s.group_id
  WHERE b.id = _userid
  INTO _areaofinterest;

  RETURN QUERY EXECUTE
    'SELECT id, zeit, typ::integer, strom, koordinate, tlp::integer,
           round(date_part(''EPOCH''::text, now() - zeit))::integer AS unixzeit
    FROM blitz.blitze
    WHERE zeit BETWEEN $1 AND $2
          AND koordinate && $3
    ORDER BY zeit;'
    USING _zeiten.startzeit, _zeiten.stopzeit, _areaofinterest;
  RETURN;
END;
$BODY$;

-- Calculate statistics
CREATE OR REPLACE FUNCTION statistic(
  start_time integer,
  end_time integer,
  coordinate_left numeric,
  coordinate_right numeric,
  coordinate_top numeric,
  coordinate_bottom numeric
) RETURNS SETOF liveblitz
    LANGUAGE plpgsql STABLE
    AS $$
DECLARE
  sql    varchar;
  result liveblitz;
  timerange record;
BEGIN

  EXECUTE 'SELECT
    to_char(now() - interval ''' || start_time ||' seconds'',''YYYY-MM-DD HH24:MI:SS'') as start_time,
    to_char(now() - interval ''' || end_time ||' seconds'',''YYYY-MM-DD HH24:MI:SS'') as end_time'
  INTO timerange;

     sql := 'SELECT
        id,
        zeit,
        typ,
        strom,
        koordinate,
        tlp,
        round(date_part(''EPOCH''::text, now() - zeit)) AS unixzeit
      FROM blitz.blitze
      WHERE zeit between ' || quote_literal(timerange.start_time) || '::timestamp with time zone
          AND ' || quote_literal(timerange.end_time) || '::timestamp with time zone
        AND koordinate && st_geometryfromText(''POLYGON((
          '|| coordinate_left ||' '|| coordinate_bottom ||',
          '|| coordinate_left ||' '|| coordinate_top ||',
          '|| coordinate_right ||' '|| coordinate_top ||',
          '|| coordinate_right ||' '|| coordinate_bottom ||'
          ,'|| coordinate_left ||' '|| coordinate_bottom ||'))'',4326)
      ORDER BY zeit';

 FOR result IN EXECUTE sql LOOP
  RETURN NEXT result;
 END LOOP;
END;
$$;

CREATE TABLE blids_schema_version
(
    version int PRIMARY KEY,
    update_date timestamp without time zone NOT NULL DEFAULT (now() AT TIME ZONE 'utc')
);

CREATE TABLE blitz.blitze
(
    id serial PRIMARY KEY,
    zeit timestamp with time zone,
    typ smallint,
    strom numeric,
    eintrag timestamp with time zone DEFAULT now(),
    koordinate geometry(Point,4326),
    grosseachse numeric,
    kleineachse numeric,
    winkelachse numeric,
    freigrade integer,
    chiquadrat numeric,
    steigzeit numeric,
    spitzenull numeric,
    sensorzahl numeric,
    hoehe numeric DEFAULT 0,
    tlp numeric DEFAULT 1,
    maxraterise numeric,
    hoeheungenau numeric,
    strokezahl integer,
    pulsezahl integer,
    CONSTRAINT blitze_zeit_typ_strom_key UNIQUE (zeit, typ, strom)
);


CREATE TABLE alarm.alarm_areas (
    id serial PRIMARY KEY,
    geom public.geometry(MultiPolygon,4326) NOT NULL,
    alarm_duration integer NOT NULL DEFAULT 0,
    radius numeric,
    center public.geometry(Point,4326),
    icon character varying,
    area numeric NOT NULL DEFAULT 0,
    inner_radius numeric,
    stroke_threshold integer
);

-- TODO_MK to be verified if the index really helps st_within checks
CREATE INDEX alarm_areas_idx
  ON alarm.alarm_areas
  USING GIST (geom);

CREATE TABLE alarm.stroke_counter (
    id integer PRIMARY KEY REFERENCES alarm.alarm_areas ON DELETE CASCADE,
    alarm boolean NOT NULL DEFAULT false,
    last_stroke timestamp with time zone,
    stroke_counter integer NOT NULL DEFAULT 0
);

CREATE FUNCTION alarm.create_stroke_counter() RETURNS trigger
    LANGUAGE plpgsql SECURITY DEFINER
    AS $$
BEGIN
  INSERT INTO alarm.stroke_counter (id) VALUES (NEW.id);
  RETURN NEW;
END;
$$;

CREATE TRIGGER alarm_area_create_stroke_counter
    AFTER INSERT ON alarm.alarm_areas
    FOR EACH ROW EXECUTE FUNCTION alarm.create_stroke_counter();

CREATE TABLE blitz.data_areas (
    id serial PRIMARY KEY,
    geom public.geometry NOT NULL
);

CREATE TABLE users.activity (
    id serial PRIMARY KEY,
    name character varying NOT NULL UNIQUE CHECK (name <> ''),
    method character varying(10) NOT NULL CHECK (method <> ''),
    destination_url character varying NOT NULL CHECK (destination_url <> '')
);

CREATE TABLE users.roles (
    id serial PRIMARY KEY,
    parent integer REFERENCES users.roles,
    name character varying NOT NULL UNIQUE CHECK (name <> '')
);

CREATE TABLE users.permissions (
    id serial PRIMARY KEY,
    name character varying NOT NULL UNIQUE CHECK (name <> '')
);

CREATE TABLE users.themes (
    id serial PRIMARY KEY,
    name character varying NOT NULL UNIQUE CHECK (name <> ''),
    data json NOT NULL
);


CREATE TABLE users.alarm_templates_sms (
    id serial PRIMARY KEY,
    alarm_subject character varying DEFAULT 'BLIDS Alarm'::character varying,
    allclear_subject character varying DEFAULT 'BLIDS Entwarnung'::character varying,
    alarm_text text,
    allclear_text text
);

CREATE TABLE users.alarm_templates_mail (
    id serial PRIMARY KEY,
    alarm_subject character varying DEFAULT 'BLIDS Alarm'::character varying,
    allclear_subject character varying DEFAULT 'BLIDS Entwarnung'::character varying,
    alarm_text text,
    allclear_text text
);

CREATE TABLE users.alarm_templates_mqtt (
    id serial PRIMARY KEY,
    alarm_subject character varying DEFAULT 'BLIDS Alarm'::character varying,
    allclear_subject character varying DEFAULT 'BLIDS Entwarnung'::character varying,
    alarm_text text,
    allclear_text text
);

CREATE TABLE users.alarm_templates_fax (
    id serial PRIMARY KEY,
    alarm_subject character varying DEFAULT 'BLIDS Alarm'::character varying,
    allclear_subject character varying DEFAULT 'BLIDS Entwarnung'::character varying,
    alarm_text text,
    allclear_text text
);

CREATE TABLE users.alarm_templates_phone (
    id serial PRIMARY KEY,
    alarm_subject character varying DEFAULT 'BLIDS Alarm'::character varying,
    allclear_subject character varying DEFAULT 'BLIDS Entwarnung'::character varying,
    alarm_text text,
    allclear_text text
);

CREATE TABLE users.alarm_config (
    id serial PRIMARY KEY,
    config json
);

CREATE TABLE users.map_layers (
    id serial PRIMARY KEY,
    geoserver_name character varying NOT NULL UNIQUE CHECK (geoserver_name <> ''),
    name character varying NOT NULL,
    reload boolean DEFAULT false NOT NULL,
    baselayer boolean DEFAULT false NOT NULL,
    timelayer boolean DEFAULT false NOT NULL,
    tick_width integer,
    strokelayer boolean DEFAULT false NOT NULL,
    snap boolean DEFAULT false,
    layer_type character varying NOT NULL,
    single_tile boolean DEFAULT false NOT NULL,
    default_name character varying DEFAULT 'Ebene'::character varying,
    external_server character varying(200),
    external_parameter json
);

CREATE TABLE users.groups (
    id serial PRIMARY KEY,
    parent integer REFERENCES users.groups,
    name character varying NOT NULL CHECK (name <> ''),
    role_id integer NOT NULL REFERENCES users.roles,
    theme_id integer REFERENCES users.themes,
    max_alarmarea integer DEFAULT 30000 NOT NULL,
    data_area_id integer REFERENCES blitz.data_areas,
    alarm_config_id integer REFERENCES users.alarm_config,
    change_alarm_config boolean DEFAULT false,

    -- Make sure names are unique within their parent group. This simple
    -- UNIQUE constraint doesn't quite suffice, see
    -- groups_name_without_parent_uniqe_idx
    UNIQUE (parent, name)
);

CREATE TABLE blitz.stroke_areas (
    id serial PRIMARY KEY,
    group_id integer NOT NULL UNIQUE REFERENCES users.groups ON DELETE CASCADE,
    geom public.geometry NOT NULL,
    CONSTRAINT enforce_dims_blitzgebiet CHECK ((public.st_ndims(geom) = 2)),
    CONSTRAINT enforce_geotype_blitzgebiet CHECK ((public.geometrytype(geom) = 'POLYGON'::text)),
    CONSTRAINT enforce_srid_blitzgebiet CHECK ((public.st_srid(geom) = 4326))
);

CREATE TABLE users.app_settings (
    id serial PRIMARY KEY,
    group_id integer NOT NULL UNIQUE REFERENCES users.groups ON DELETE CASCADE,
    max_display_time integer,
    archive_days integer DEFAULT 0 NOT NULL,
    archive_since timestamp with time zone,
    max_zoom integer DEFAULT 14 NOT NULL,
    min_zoom integer DEFAULT 6 NOT NULL,
    live_blids boolean DEFAULT false NOT NULL,
    animation boolean DEFAULT false NOT NULL,
    sound boolean DEFAULT false NOT NULL,
    path character varying DEFAULT 1 NOT NULL,
    max_alarms integer DEFAULT 1 NOT NULL,
    max_alarm_areas integer DEFAULT 1 NOT NULL,
    max_alarm_users integer DEFAULT 1 NOT NULL,
    statistic_windowed boolean DEFAULT true NOT NULL
);

-- Additional unique index to make sure the names of the groups without
-- parent are unique.
CREATE UNIQUE INDEX groups_name_without_parent_unique_idx
                 ON users.groups (name)
              WHERE parent is NULL;


CREATE TABLE users.users (
    id serial PRIMARY KEY,
    name character varying NOT NULL UNIQUE, CHECK (name <> ''),
    password character varying,
    group_id integer NOT NULL REFERENCES users.groups,
    start_date timestamp with time zone DEFAULT now() NOT NULL,
    stop_date timestamp with time zone DEFAULT (now() + '1 year'::interval) NOT NULL,
    first_name character varying DEFAULT '' NOT NULL,
    last_name character varying DEFAULT '' NOT NULL,
    company character varying DEFAULT '' NOT NULL,
    division character varying DEFAULT '' NOT NULL,
    street character varying DEFAULT '' NOT NULL,
    zip character varying DEFAULT '' NOT NULL,
    city character varying DEFAULT '' NOT NULL,
    phonenumber character varying DEFAULT '' NOT NULL,
    annotation character varying DEFAULT '' NOT NULL,
    mobilenumber character varying DEFAULT '' NOT NULL,
    email character varying DEFAULT '' NOT NULL
);

CREATE TABLE users.alarm_settings (
    id integer PRIMARY KEY REFERENCES users.users ON DELETE CASCADE,
    alarm_template_mail_id int REFERENCES users.alarm_templates_mail,
    alarm_template_sms_id int REFERENCES users.alarm_templates_sms,
    alarm_template_mqtt_id int REFERENCES users.alarm_templates_mqtt,
    alarm_template_fax_id int REFERENCES users.alarm_templates_fax,
    alarm_template_phone_id int REFERENCES users.alarm_templates_phone,
    sms character varying,
    email character varying,
    phone character varying,
    fax character varying,
    mqtt character varying,
    start_date timestamp with time zone NOT NULL,
    stop_date timestamp with time zone NOT NULL,
    start_time time DEFAULT '00:00:00'::time NOT NULL,
    stop_time time DEFAULT '00:00:00'::time NOT NULL,
    time_zone text DEFAULT 'UTC' NOT NULL,
    mon boolean DEFAULT true NOT NULL,
    tue boolean DEFAULT true NOT NULL,
    wed boolean DEFAULT true NOT NULL,
    thu boolean DEFAULT true NOT NULL,
    fri boolean DEFAULT true NOT NULL,
    sat boolean DEFAULT true NOT NULL,
    sun boolean DEFAULT true NOT NULL,
    only_log boolean DEFAULT false NOT NULL,
    stroke_protocol boolean DEFAULT false NOT NULL,
    stroke_as_csv boolean DEFAULT false NOT NULL
);


CREATE TABLE users.profile (
    id serial PRIMARY KEY,
    client_config json,
    user_id integer NOT NULL UNIQUE REFERENCES users.users ON DELETE CASCADE
);

CREATE TABLE alarm.alarm_area_unit (
    id serial PRIMARY KEY,
    name character varying NOT NULL CHECK (name <> ''),
    group_id integer NOT NULL REFERENCES users.groups,
    alarm boolean NOT NULL DEFAULT FALSE,
    alarm_timestamp timestamp with time zone,
    is_extended boolean NOT NULL DEFAULT FALSE,
    notified boolean NOT NULL DEFAULT FALSE,

    UNIQUE (group_id, name)
);

CREATE TABLE alarm.alarms (
    id serial PRIMARY KEY,
    alarm_area_unit_id integer REFERENCES alarm.alarm_area_unit,
    user_id integer REFERENCES users.users ON DELETE CASCADE
);

CREATE TABLE alarm.alarm_area_map (
    alarm_area_id integer REFERENCES alarm.alarm_areas,
    alarm_area_unit_id integer REFERENCES alarm.alarm_area_unit,

    PRIMARY KEY (alarm_area_id, alarm_area_unit_id)
);

CREATE TABLE alarm.notifications (
    id serial PRIMARY KEY,
    alarm integer NOT NULL REFERENCES alarm.alarms,
    area integer NOT NULL REFERENCES alarm.alarm_areas,
    date timestamp with time zone DEFAULT now(),
    done timestamp with time zone,
    type alarm.alarm_type,
    status alarm.send_state,
    status_info text
);

CREATE TABLE users.group_map_layer (
    id serial PRIMARY KEY,
    group_id integer REFERENCES users.groups ON DELETE CASCADE,
    map_layer_id integer REFERENCES users.map_layers,
    reload_time integer DEFAULT '-1'::integer NOT NULL,
    display_name character varying DEFAULT 'Layer'::character varying NOT NULL,
    feature_info boolean DEFAULT false NOT NULL,
    checked_on_login boolean DEFAULT false NOT NULL,
    permanent boolean DEFAULT false NOT NULL,
    cql_filter json,
    popup_template character varying,
    zindex integer
);

CREATE TABLE users.user_roles (
    user_id integer REFERENCES users.users ON DELETE CASCADE,
    role_id integer REFERENCES users.roles,

    PRIMARY KEY (user_id, role_id)
);

CREATE TABLE users.permission_activity (
    permission_id integer REFERENCES users.permissions,
    activity_id integer REFERENCES users.activity,

    PRIMARY KEY (permission_id, activity_id)
);

CREATE TABLE users.role_permissions (
    role_id integer REFERENCES users.roles ON DELETE CASCADE,
    permission_id integer REFERENCES users.permissions,

    PRIMARY KEY (role_id, permission_id)
);

CREATE TABLE users.host_theme (
    host character varying PRIMARY KEY CHECK (host <> ''),
    theme json NOT NULL DEFAULT '{}'::JSON
);

CREATE TABLE users.session_log (
    id serial PRIMARY KEY,
    logged_in timestamp with time zone,
    logged_out timestamp with time zone,
    user_id integer REFERENCES users.users
);

CREATE TABLE users.sessions (
    id character varying(32) NOT NULL,
    user_id serial REFERENCES users.users(id),
    access_time timestamp with time zone,
    data text
);

CREATE FUNCTION users.invalidate_sessions() RETURNS void
AS $function$
    DELETE FROM users.sessions
        WHERE access_time < CURRENT_TIMESTAMP - '1h'::interval;
$function$ LANGUAGE SQL;

CREATE TABLE users.logging (
    id serial PRIMARY KEY,
    user_id integer NOT NULL REFERENCES users.users,
    date timestamp with time zone DEFAULT now() NOT NULL,
    text character varying NOT NULL,
    ip character varying(45) DEFAULT '-'::character varying NOT NULL
);

CREATE TABLE users.host_themes (
    id serial PRIMARY KEY,
    host character varying NOT NULL,
    data json NOT NULL
);

CREATE TABLE users.password_reset_requests (
    hash varchar(32) PRIMARY KEY,
    issued timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
    user_id int4 NOT NULL UNIQUE REFERENCES users.users(id)
        ON DELETE CASCADE ON UPDATE CASCADE
);

CREATE FUNCTION users.invalidate_password_reset_requests() RETURNS void
AS $function$
    DELETE FROM users.password_reset_requests
        WHERE issued < CURRENT_TIMESTAMP - '12h'::interval;
$function$ LANGUAGE SQL;

CREATE TRIGGER notify_on_stroke
    BEFORE INSERT ON blitz.blitze
    FOR EACH ROW EXECUTE PROCEDURE blitz.notify_stroke();

CREATE TRIGGER update_area_on_stroke
    AFTER INSERT ON blitz.blitze
    FOR EACH ROW EXECUTE PROCEDURE blitz.insert_stroke();

CREATE TRIGGER alert_areas_on_stroke
    BEFORE UPDATE ON alarm.stroke_counter
    FOR EACH ROW EXECUTE PROCEDURE alarm.alarm();

CREATE TRIGGER notify_alarms
    BEFORE INSERT ON alarm.notifications
    FOR EACH ROW EXECUTE PROCEDURE alarm.notify_alarm();

CREATE TRIGGER recalculate_surface_area
    BEFORE INSERT OR UPDATE OF geom ON alarm.alarm_areas
    FOR EACH ROW
    EXECUTE PROCEDURE alarm.update_surface_area();
