CREATE EXTENSION pg_cron;

SELECT cron.schedule('*/15 * * * *',
    $$SELECT alarm.expire_alarms()$$);
SELECT cron.schedule('*/15 * * * *',
    $$SELECT users.invalidate_sessions()$$);
SELECT cron.schedule('*/15 * * * *',
    $$SELECT users.invalidate_password_reset_requests()$$);
