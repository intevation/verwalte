-- Do not change the name of activities. Changes break the functionality of the client.
-- If a change is necessary, make sure to change it in client, too.
COPY users.activity (id, name, method, destination_url) FROM stdin;
0	login	POST	/login
1	get user	GET	/user
2	create user	POST	/user
3	update user	PUT	/user
4	delete user	DELETE	/user/{id:[0-9]+}
5	get group	GET	/group
6	create group	POST	/group
7	update group	PUT	/group
8	delete group	DELETE	/group/{id:[0-9]+}
9	get role	GET	/role
10	create role	POST	/role
11	update role	PUT	/role
12	delete role	DELETE	/role/{id:[0-9]+}
13	get permission	GET	/permission
14	get user id	GET	/user/{id:[0-9]+}
15	get group id	GET	/group/{id:[0-9]+}
16	get role id	GET	/role/{id:[0-9]+}
17	get permission id	GET	/permission/{pid:[0-9]+}
18	get role permission	GET	/role/{id:[0-9]+}/permission
19	add role permission	PUT	/role/{id:[0-9]+}/permission
20	get role permission id	GET	/role/{id:[0-9]+}/permission/{pid:[0-9]+}
21	delete role permission	DELETE	/role/{id:[0-9]+}/permission/{pid:[0-9]+}
22	update profile	PUT	/user/{id:[0-9]+}/profile
23	update appsetting	PUT	/group/{id:[0-9]+}/settings
24	update alarmsetting	PUT	/user/{id:[0-9]+}/alarmsettings
25	get alarmsetting	GET	/user/{id:[0-9]+}/alarmsettings
26	get appsetting	GET	/group/{id:[0-9]+}/settings
27	get strokearea	GET	/group/{id:[0-9]+}/strokearea
28	update strokearea	PUT	/group/{id:[0-9]+}/strokearea
29	create strokearea	POST	/group/{id:[0-9]+}/strokearea
30	get user alarm	GET	/user/{id:[0-9]+}/alarm
31	add user alarm	PUT	/user/{id:[0-9]+}/alarm
32	get user alarm id	GET	/user/{id:[0-9]+}/alarm/{aid:[0-9]+}
33	delete user alarm	DELETE	/user/{id:[0-9]+}/alarm/{aid:[0-9]+}
34	get alarm	GET	/group/{id:[0-9]+}/alarm
35	create alarm	POST	/group/{id:[0-9]+}/alarm
36	update alarm	PUT	/group/{id:[0-9]+}/alarm
37	delete alarm	DELETE	/group/{id:[0-9]+}/alarm/{aid:[0-9]+}
38	get alarm id	GET	/group/{id:[0-9]+}/alarm/{aid:[0-9]+}
39	get alarmarea	GET	/group/{id:[0-9]+}/alarm/{aid:[0-9]+}/area
40	create alarmarea	POST	/group/{id:[0-9]+}/alarm/{aid:[0-9]+}/area
41	update alarmarea	PUT	/group/{id:[0-9]+}/alarm/{aid:[0-9]+}/area
42	delete alarmarea	DELETE	/group/{id:[0-9]+}/alarm/{aid:[0-9]+}/area/{aaid:[0-9]+}
43	get alarmarea id	GET	/group/{id:[0-9]+}/alarm/{aid:[0-9]+}/area/{aaid:[0-9]+}
44	get layer	GET	/layer
45	update layer	PUT	/layer
46	create layer	POST	/layer
47	get layer id	GET	/layer/{id:[0-9]+}
48	delete layer	DELETE	/layer/{id:[0-9]+}
49	get group layer	GET	/group/{id:[0-9]+}/maplayer
50	update group layer	PUT	/group/{id:[0-9]+}/maplayer
51	create group layer	POST	/group/{id:[0-9]+}/maplayer
52	get group layer id	GET	/group/{id:[0-9]+}/maplayer/{lid:[0-9]+}
53	delete group layer	DELETE	/group/{id:[0-9]+}/maplayer/{lid:[0-9]+}
54	get statistic	GET	/statistic
55	connect stroke stream	GET	/strokestream
56	close stroke stream	GET	/strokestreamclose
57	connect alarm stream	GET	/alarmstream
58	close alarm stream	GET	/alarmstreamclose
59	get alarmtemplates mail	GET	/template/mail
60	get alarmtemplates sms	GET	/template/sms
61	get alarmtemplates fax	GET	/template/fax
62	get alarmtemplates mqtt	GET	/template/mqtt
63	get alarmtemplates phone	GET	/template/phone
64	get userlog	GET	/userlog
65	get version	GET	/version
66	get hosttheme	GET	/hosttheme
67	get theme	GET	/theme
68	get layers by group	GET	/group/{id:[0-9]+}/layer
\.
