CREATE OR REPLACE FUNCTION blitz.blitz_userid(
_userid integer,
_zeit1 integer,
_zeit2 integer,
_co_left numeric,
_co_right numeric,
_co_top numeric,
_co_bottom numeric)
    RETURNS SETOF liveblitz
    LANGUAGE 'plpgsql'
    COST 100
    STABLE PARALLEL UNSAFE
    ROWS 1000
AS $BODY$
DECLARE
  _zeiten         record;
  _areaofinterest geometry;
BEGIN
  SELECT now() - make_interval(secs => _zeit1) AS startzeit,
         now() - make_interval(secs => _zeit2) AS stopzeit
    INTO _zeiten;

  SELECT st_intersection(
    st_transform(st_makeenvelope(_co_left, _co_top, _co_right, _co_bottom, 3857), 4326),
    s.geom)
  FROM users.users b JOIN users.groups g ON b.group_id = g.id JOIN blitz.stroke_areas s on g.id = s.group_id
  WHERE b.id = _userid
  INTO _areaofinterest;

  RETURN QUERY EXECUTE
    'SELECT id, zeit, typ::integer, strom, koordinate, tlp::integer,
           round(date_part(''EPOCH''::text, now() - zeit))::integer AS unixzeit
    FROM blitz.blitze
    WHERE zeit BETWEEN $1 AND $2
          AND koordinate && $3
    ORDER BY zeit;'
    USING _zeiten.startzeit, _zeiten.stopzeit, _areaofinterest;
  RETURN;
END;
$BODY$;